cardsContainer.addEventListener('dragstart', event => {
    event.target.classList.add('selected');
});
cardsContainer.addEventListener('dragend', event => {
    let activeElement = event.target;
    activeElement.classList.remove('selected');
    let activeElementId = activeElement.querySelector('.card-ID').textContent;
    let activeElementIdNum = activeElementId.match(/\d+/g).toString();
    let activeItem = cardsArray.find(item => item.visitId === activeElementIdNum);
    if((activeElement.closest('.cards-container-closed')
        && activeItem.status === 'open')
        ||
        (activeElement.closest('.cards-container-open')
            && activeItem.status === 'closed'))
    {
        activeItem.changeStatus();
    }
});

const getNextElement = (cursorPosition, currentElement) => {
    const currentElementCoord = currentElement.getBoundingClientRect();
    const currentElementCenter = currentElementCoord.y + currentElementCoord.height / 2;
    const nextElement = (cursorPosition < currentElementCenter) ? currentElement : currentElement.nextElementSibling;
    return nextElement;
};

openCardsContainer.addEventListener('dragover', event => {
    event.preventDefault();
    const activeElement = cardsContainer.querySelector('.selected');
    const currentElement = event.target;
    const isMovable = activeElement !== currentElement && currentElement.classList.contains('card');
    if(isMovable) {
        const nextElement = getNextElement(event.clientY, currentElement);
        if (
            nextElement &&
            activeElement === nextElement.previousElementSibling ||
            activeElement === nextElement
        ) {
            return;
        }
        openCardsContainer.insertBefore(activeElement, nextElement);
    } else if (currentElement === openCardsContainer) {
        openCardsContainer.append(activeElement);
    }
});

closedCardsContainer.addEventListener('dragover', event => {
    event.preventDefault();
    const activeElement = cardsContainer.querySelector('.selected');
    const currentElement = event.target;
    const isMovable = activeElement !== currentElement && currentElement.classList.contains('card');
    if(isMovable) {
        const nextElement = getNextElement(event.clientY, currentElement);
        if (
            nextElement &&
            activeElement === nextElement.previousElementSibling ||
            activeElement === nextElement
        ) {
            return;
        }
        closedCardsContainer.insertBefore(activeElement, nextElement);
    } else if (currentElement === closedCardsContainer) {
        closedCardsContainer.append(activeElement);
    }
});
