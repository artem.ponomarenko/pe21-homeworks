class Visit {
    constructor(id, doctor, purpose, description, patient, priority, status, pressure, index, last, diseases, age) {
        this.visitId = id;
        this.doctor = doctor;
        this.purpose = purpose;
        this.description = description;
        this.patient = patient;
        this.priority = priority;
        this.status = status;
        this.diseases = diseases;
        this.last = last;
        this.index = index;
        this.pressure = pressure;
        this.age = age;
    }
    changeStatus() {
        this.status === 'open' ? this.status = 'closed' : this.status = 'open';
        this.change(this.collectData());
    }
    change(visitData) {
        this.visitCard.classList.remove(this.priority);
        if(visitData) {
            editCardOnServer(this.visitId, visitData)
                .then(json => {
                    this.editCard(json);
                    this.checkStatus();
                });
        }
    }
    hideCard() {
        this.visitCard.classList.add('hidden');
    }
    showCard() {
        this.visitCard.classList.remove('hidden');
    }
    collectData() {
        return {
            id: this.visitId,
            purpose: this.purpose,
            doctor: this.doctor,
            description: this.description,
            age: this.age,
            priority: this.priority,
            patient: this.patient,
            status: this.status,
            pressure: this.pressure,
            index: this.index,
            last: this.last,
            diseases: this.diseases
        };
    }
    checkStatus() {
        if(this.status === 'closed') {
            this.visitCard.classList.add('closed');
            this.cardChangeButton.disabled = true;
            this.cardPriority.disabled = true;
        } else {
            this.visitCard.classList.remove('closed');
            this.cardChangeButton.disabled = false;
            this.cardPriority.disabled = false;
        }
    }
    deleteCard(id) {
        deleteCardOnServer(id)
            .then(json => {
                if(json.status === 'Success') {
                    this.visitCard.remove();
                    cardsArray.splice(cardsArray.indexOf(this), 1);
                    searchForm.searchCheck();
                } else {
                    console.error('Error deleting card');
                }
            });
    }
    toggleButtons() {
        this.cardId.classList.toggle('hidden');
        this.cardDeleteButton.classList.toggle('hidden');
        this.cardChangeButton.classList.toggle('hidden');
        this.cardEditButton.classList.toggle('card-show-more-button');
        this.cardEditButton.textContent === 'Edit' ? this.cardEditButton.textContent = 'Close' : this.cardEditButton.textContent = 'Edit';
    }
    closeButtonsOnClick() {
        this.toggleButtons();
        document.addEventListener('click', event => {
            if(this.cardEditButton.classList.contains('card-show-more-button')
                && event.target !== this.cardDeleteButton
                && event.target !== this.cardEditButton
                && event.target !== this.cardChangeButton) {
                this.toggleButtons();
            }
        });
    }
    toggleShowMore() {
        this.cardAddInfoContainer.classList.toggle('hidden');
        this.cardShowMoreButton.textContent === 'Show more' ? this.cardShowMoreButton.textContent = 'Show less' : this.cardShowMoreButton.textContent = 'Show more';
    }
    closeShowMoreOnClick() {
        this.toggleShowMore();
        document.addEventListener('click', event => {
            if(!this.cardAddInfoContainer.classList.contains('hidden')
                && !this.visitCard.contains(event.target)) {
                this.toggleShowMore();
            }
        });
    }
    renderCardButtons() {
        this.cardButtonsCont = document.createElement('div');
        this.cardButtonsCont.classList.add('card-buttons-container');

        this.cardId = document.createElement('p');
        this.cardId.classList.add('card-ID');
        this.cardId.textContent = 'ID ' + this.visitId;

        this.cardDeleteButton = document.createElement('button');
        this.cardDeleteButton.classList.add('card-edit-button','red-bg', 'hidden');
        this.cardDeleteButton.textContent = 'Delete';
        this.cardDeleteButton.addEventListener('click', ()=> {
            this.deleteCard(this.visitId);
        });

        this.cardChangeButton = document.createElement('button');
        this.cardChangeButton.classList.add('card-edit-button', 'hidden');
        this.cardChangeButton.textContent = 'Edit';
        this.cardChangeButton.addEventListener('click', ()=> {
            this.edit();
        });

        this.cardEditButton = document.createElement('button');
        this.cardEditButton.classList.add('card-edit-button');
        this.cardEditButton.textContent = 'Edit';
        this.cardEditButton.addEventListener('click', ()=> {
            this.closeButtonsOnClick();
        });

        this.cardShowMoreButton = document.createElement('button');
        this.cardShowMoreButton.classList.add('card-show-more-button');
        this.cardShowMoreButton.textContent = 'Show more';
        this.cardShowMoreButton.addEventListener('click',()=> {
            this.closeShowMoreOnClick();
        });

        this.cardButtonsCont.append(
            this.cardId,
            this.cardDeleteButton,
            this.cardChangeButton,
            this.cardEditButton,
            this.cardShowMoreButton);

        return this.cardButtonsCont;
    }
    edit() {
        let editForm = new Form();
        let editFormDOM,
            editButton;
        switch (this.doctor) {
            case 'Cardiologist' : {
                editFormDOM = editForm.renderCardiologist(this.doctor, this.collectData());
                editFormDOM.classList.add('any-form', 'left');
                editButton = editForm.renderEditButton();
                editButton.addEventListener('click', event => {
                    event.preventDefault();
                    if(editForm.sendCardiologist()) {
                        editCardOnServer(this.visitId, editForm.sendCardiologist())
                            .then(json => this.editCard(json))
                            .then(() => editFormDOM.remove());
                    }
                });
            }
                break;
            case 'Dentist' : {
                editFormDOM = editForm.renderDentist(this.doctor, this.collectData());
                editFormDOM.classList.add('any-form', 'left');
                editButton = editForm.renderEditButton();
                editButton.addEventListener('click', event => {
                    event.preventDefault();
                    if(editForm.sendDentist()) {
                        editCardOnServer(this.visitId, editForm.sendDentist())
                            .then(json => this.editCard(json))
                            .then(() => editFormDOM.remove());
                    }
                });
            }
                break;
            case 'Therapist' : {
                editFormDOM = editForm.renderTherapist(this.doctor, this.collectData());
                editFormDOM.classList.add('any-form', 'left');
                editButton = editForm.renderEditButton();
                editButton.addEventListener('click', event => {
                    event.preventDefault();
                    if(editForm.sendTherapist()) {
                        editCardOnServer(this.visitId, editForm.sendTherapist())
                            .then(json => this.editCard(json))
                            .then(() => editFormDOM.remove());
                    }
                });
            }
                break;
        }
        let cancelBtn = new FormButton('Cancel', 'cancel-button');
        let cancelButton = cancelBtn.render();
        cancelButton.addEventListener('click', ()=> {
            editFormDOM.remove();
        });
        editFormDOM.append(editButton, cancelButton);

        formsContainer.append(editFormDOM);
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;

        setTimeout(()=> {
            document.addEventListener('click', event => {
                if(!editFormDOM.contains(event.target)) {
                    editFormDOM.remove();
                }
            });
        }, 5);
    }
}

class VisitTherapist extends Visit {
    constructor({id, doctor, purpose, description, patient, priority, status, pressure, index, last, diseases, age}) {
        super(id, doctor, purpose, description, patient, priority, status, pressure, index, last, diseases, age);
    }
    render() {
        this.visitCard = document.createElement('div');
        this.visitCard.classList.add('card', 'therapist', this.priority, 'open');
        this.visitCard.draggable = true;


        this.cardName = document.createElement('h3');
        this.cardName.classList.add('h3-card-name', 'green-font');
        this.cardName.textContent = this.doctor;

        this.cardPatient = document.createElement('h4');
        this.cardPatient.classList.add('h4-card-patient');
        this.cardPatient.textContent = this.patient;

        this.cardAddInfoContainer = document.createElement('div');
        this.cardAddInfoContainer.classList.add('add-card-info-container', 'hidden');

        this.cardPurpose = document.createElement('p');
        this.cardPurpose.classList.add('card-info', 'purpose');
        this.cardPurpose.textContent = this.purpose;

        this.cardDescription = document.createElement('p');
        this.cardDescription.classList.add('card-info', 'description');
        this.cardDescription.textContent = this.description;

        this.selectPriority = new Select('Select priority', priorityOptionsList);
        this.cardPriority = this.selectPriority.render();
        this.cardPriority.value = this.priority;
        this.cardPriority.addEventListener('change', ()=> {
            this.visitCard.classList.remove(this.priority);
            this.priority = this.cardPriority.value;
            this.change(this.collectData());
        });

        this.cardAge = document.createElement('p');
        this.cardAge.classList.add('card-info', 'age');
        this.cardAge.textContent = this.age;

        this.cardAddInfoContainer.append(
            this.cardPurpose,
            this.cardDescription,
            this.cardAge,
            this.cardPriority
        );

        this.visitCard.append(
            this.cardName,
            this.cardPatient,
            this.cardAddInfoContainer,
            this.renderCardButtons()
            );

        this.checkStatus();
        return this.visitCard;
    }
    editCard({priority, doctor, patient, purpose, description, age, status}) {
        this.doctor = doctor;
        this.priority = priority;
        this.patient = patient;
        this.purpose = purpose;
        this.description = description;
        this.age = age;
        this.status = status;
        this.visitCard.classList.add(this.priority);
        this.cardName.textContent = doctor;
        this.cardPatient.textContent = patient;
        this.cardPurpose.textContent = purpose;
        this.cardDescription.textContent = description;
        this.cardPriority.value = priority;
        this.cardAge.textContent = age;
        this.checkStatus();
    }
}

class VisitDentist extends Visit {
    constructor({id, doctor, purpose, description, patient, priority, status, pressure, index, last, diseases, age}) {
        super(id, doctor, purpose, description, patient, priority, status, pressure, index, last, diseases, age);
    }
    render() {
        this.visitCard = document.createElement('div');
        this.visitCard.classList.add('card', 'dentist', this.priority, 'open');
        this.visitCard.draggable = true;


        this.cardName = document.createElement('h3');
        this.cardName.classList.add('h3-card-name', 'blue-font');
        this.cardName.textContent = this.doctor;

        this.cardPatient = document.createElement('h4');
        this.cardPatient.classList.add('h4-card-patient');
        this.cardPatient.textContent = this.patient;

        this.cardAddInfoContainer = document.createElement('div');
        this.cardAddInfoContainer.classList.add('add-card-info-container', 'hidden');

        this.cardPurpose = document.createElement('p');
        this.cardPurpose.classList.add('card-info', 'purpose');
        this.cardPurpose.textContent = this.purpose;

        this.cardDescription = document.createElement('p');
        this.cardDescription.classList.add('card-info', 'description');
        this.cardDescription.textContent = this.description;

        this.selectPriority = new Select('Select priority', priorityOptionsList);
        this.cardPriority = this.selectPriority.render();
        this.cardPriority.value = this.priority;
        this.cardPriority.addEventListener('change', ()=> {
            this.visitCard.classList.remove(this.priority);
            this.priority = this.cardPriority.value;
            this.change(this.collectData());
        });

        this.cardLast = document.createElement('p');
        this.cardLast.classList.add('card-info', 'last');
        this.cardLast.textContent = this.last;

        this.cardAddInfoContainer.append(
            this.cardPurpose,
            this.cardDescription,
            this.cardLast,
            this.cardPriority
        );

        this.visitCard.append(
            this.cardName,
            this.cardPatient,
            this.cardAddInfoContainer,
            this.renderCardButtons()
        );

        this.checkStatus();
        return this.visitCard;
    }
    editCard({priority, doctor, patient, purpose, description, last, status}) {
        this.doctor = doctor;
        this.priority = priority;
        this.patient = patient;
        this.purpose = purpose;
        this.description = description;
        this.last = last;
        this.status = status;
        this.visitCard.classList.add(this.priority);
        this.cardName.textContent = doctor;
        this.cardPatient.textContent = patient;
        this.cardPurpose.textContent = purpose;
        this.cardDescription.textContent = description;
        this.cardPriority.value = priority;
        this.cardLast.textContent = last;
        this.checkStatus();
    }
}

class VisitCardiologist extends Visit {
    constructor({id, doctor, purpose, description, patient, priority, status, pressure, index, last, diseases, age}) {
        super(id, doctor, purpose, description, patient, priority, status, pressure, index, last, diseases, age);
    }
    render() {
        this.visitCard = document.createElement('div');
        this.visitCard.classList.add('card', 'cardiologist', this.priority, 'open');
        this.visitCard.draggable = true;


        this.cardName = document.createElement('h3');
        this.cardName.classList.add('h3-card-name', 'red-font');
        this.cardName.textContent = this.doctor;

        this.cardPatient = document.createElement('h4');
        this.cardPatient.classList.add('h4-card-patient');
        this.cardPatient.textContent = this.patient;

        this.cardAddInfoContainer = document.createElement('div');
        this.cardAddInfoContainer.classList.add('add-card-info-container', 'hidden');

        this.cardPurpose = document.createElement('p');
        this.cardPurpose.classList.add('card-info', 'purpose');
        this.cardPurpose.textContent = this.purpose;

        this.cardIndex = document.createElement('p');
        this.cardIndex.classList.add('card-info', 'index');
        this.cardIndex.textContent = this.index;

        this.cardPressure = document.createElement('p');
        this.cardPressure.classList.add('card-info', 'pressure');
        this.cardPressure.textContent = this.pressure;

        this.cardDiseases = document.createElement('p');
        this.cardDiseases.classList.add('card-info', 'diseases');
        this.cardDiseases.textContent = this.diseases;

        this.cardDescription = document.createElement('p');
        this.cardDescription.classList.add('card-info', 'description');
        this.cardDescription.textContent = this.description;

        this.selectPriority = new Select('Select priority', priorityOptionsList);
        this.cardPriority = this.selectPriority.render();
        this.cardPriority.value = this.priority;
        this.cardPriority.addEventListener('change', ()=> {
            this.visitCard.classList.remove(this.priority);
            this.priority = this.cardPriority.value;
            this.change(this.collectData());
        });

        this.cardAge = document.createElement('p');
        this.cardAge.classList.add('card-info', 'age');
        this.cardAge.textContent = this.age;

        this.cardAddInfoContainer.append(
            this.cardPurpose,
            this.cardDescription,
            this.cardIndex,
            this.cardPressure,
            this.cardAge,
            this.cardDiseases,
            this.cardPriority
        );

        this.visitCard.append(
            this.cardName,
            this.cardPatient,
            this.cardAddInfoContainer,
            this.renderCardButtons()
        );
        this.checkStatus();
        return this.visitCard;
    }
    editCard({priority, doctor, patient, purpose, description, age, status, index, pressure, diseases}) {
        this.doctor = doctor;
        this.priority = priority;
        this.patient = patient;
        this.purpose = purpose;
        this.description = description;
        this.index = index;
        this.pressure = pressure;
        this.diseases = diseases;
        this.age = age;
        this.status = status;
        this.visitCard.classList.add(this.priority);
        this.cardName.textContent = doctor;
        this.cardPatient.textContent = patient;
        this.cardPurpose.textContent = purpose;
        this.cardDescription.textContent = description;
        this.cardPriority.value = priority;
        this.cardAge.textContent = age;
        this.cardPressure.textContent = pressure;
        this.cardDiseases.textContent = diseases;
        this.cardIndex.textContent = index;
        this.checkStatus();
    }
}