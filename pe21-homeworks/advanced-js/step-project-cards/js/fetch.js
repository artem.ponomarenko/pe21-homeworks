async function getCards() {
    let response = await fetch('http://cards.danit.com.ua/cards', {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${loginToken}`
        }
    });
    return await response.json();
}

async function editCardOnServer(id, body) {
    let response = await fetch(`http://cards.danit.com.ua/cards/${id}`, {
        method: 'PUT',
        headers: {
            'Authorization': `Bearer ${loginToken}`
        },
        body: JSON.stringify(body)
    });
    return await response.json();
}

async function deleteCardOnServer(id) {
    let response = await fetch(`http://cards.danit.com.ua/cards/${id}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${loginToken}`
        }
    });
    return await response.json();
}

async function createNewVisit(body) {
    let response = await fetch('http://cards.danit.com.ua/cards', {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${loginToken}`
        },
        body: JSON.stringify(body)
    });
    return await response.json();
}