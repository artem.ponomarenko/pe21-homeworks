class LoginForm {
    constructor(formName, position) {
        this.name = formName;
        this.position = position;
        this.emailInp = new Input('email', 'text', 'Enter email');
        this.passwordInp = new Input('password', 'password', 'Enter password');
        this.loginBtn = new FormButton('Login', 'input-button');
        this.cancelBtn = new FormButton('Cancel', 'cancel-button');
    }
    render(parent) {
        this.form = document.createElement('form');
        this.form.classList.add('any-form');
        this.form.classList.add(this.position);
        this.form.classList.add('hidden');
        this.formName = document.createElement('h2');
        this.formName.classList.add('h2-style');
        this.formName.textContent = this.name;
        this.email = this.emailInp.render();
        this.password = this.passwordInp.render();
        this.loginButton = this.loginBtn.render();
        this.cancelButton = this.cancelBtn.render();
        this.loginButton.addEventListener('click', event => {
            event.preventDefault();
            this.sendLogin();
        });
        this.cancelButton.addEventListener('click', event => {
            event.preventDefault();
            this.hide()
        });
        this.form.append(
            this.formName,
            this.email,
            this.password,
            this.loginButton,
            this.cancelButton,
            );
        parent.appendChild(this.form);
    }
    show() {
        this.form.classList.remove('hidden');
    }
    hide() {
        this.form.classList.add('hidden');
        this.formName.textContent = this.name;
        this.formName.classList.remove('red-font');
        this.loginButton.textContent = 'Login';
        this.loginButton.classList.remove('red-bg');
        this.email.value = null;
        this.password.value = null;
    }
    tryAgain(message) {
        this.formName.textContent = message;
        this.formName.classList.add('red-font');
        this.loginButton.textContent = 'Try again';
        this.loginButton.classList.add('red-bg');
    }
    sendLogin() {
        const loginResults = new FormData(this.form);
        const loginData = {
            email: loginResults.get('email'),
            password: loginResults.get('password')
        };
        if(loginData.email && loginData.password) {
            this.getLogin(loginData);
        }
    }
    getLogin(data) {
        fetch('http://cards.danit.com.ua/login', {
            method: 'POST',
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(json => {
                if(json.status === 'Success') {
                    sessionStorage.setItem('token', json.token);
                    this.hide();
                    checkToken();
                } else if(json.status === 'Error') {
                    this.tryAgain(json.message);
                }
            });
    }
}

class Modal {
    constructor(name, position) {
        this.name = name;
        this.position = position;
        this.doctors = [
            {
                name: 'Therapist',
                value: 'Therapist'
            },
            {
                name: 'Dentist',
                value: 'Dentist'
            },
            {
                name: 'Cardiologist',
                value: 'Cardiologist'
            }
        ];
        this.selectDoctor = new Select('Select doctor', this.doctors);
        this.cancelBtn = new FormButton('Cancel', 'cancel-button');
    }
    render(parent) {
        this.modal = document.createElement('div');
        this.modal.classList.add('any-form', 'hidden', this.position);
        this.modalName = document.createElement('h2');
        this.modalName.textContent = this.name;
        this.modalName.classList.add('h2-style');
        this.doctor = this.selectDoctor.render();
        this.doctor.addEventListener('change', () => {
            this.formContainer.innerHTML = '';
            this.addForm();
        });
        this.formContainer = document.createElement('div');
        this.cancelButton = this.cancelBtn.render();
        this.cancelButton.addEventListener('click',event => {
           event.preventDefault();
           this.hide();
        });
        this.modal.append(
            this.modalName,
            this.doctor,
            this.formContainer,
            this.cancelButton
        );
        parent.append(this.modal);
    }
    showNewModal() {
        this.addForm();
        this.modal.classList.remove('hidden');
    }
    hide() {
        this.modal.classList.add('hidden');
        this.modal.innerHTML = '';
        this.doctor.value = this.doctors[0].value;
    }
    addForm() {
        switch (this.doctor.value) {
            case this.doctors[0].value: {
                this.therapistForm = new Form();
                this.therapistDOM = this.therapistForm.renderTherapist(this.doctors[0].name);
                this.therapistForm.renderCreateTherapistButton(this.therapistDOM);
                this.formContainer.append(this.therapistDOM);
            }
            break;
            case this.doctors[1].value: {
                this.dentistForm = new Form();
                this.dentistDOM = this.dentistForm.renderDentist(this.doctors[1].name);
                this.dentistForm.renderCreateDentistButton(this.dentistDOM);
                this.formContainer.append(this.dentistDOM);
            }
            break;
            case this.doctors[2].value: {
                this.cardiologistForm = new Form();
                this.cardiologistDOM = this.cardiologistForm.renderCardiologist(this.doctors[2].name);
                this.cardiologistForm.renderCreateCardiologistButton(this.cardiologistDOM);
                this.formContainer.append(this.cardiologistDOM);
            }
            break;
        }
    }
}

class Form {
    constructor() {
        this.patientInp = new Input('patient', 'text', 'Enter patient name');
        this.purposeInp = new Input('purpose', 'text', 'Enter visit purpose');
        this.descInp = new TextArea('desc', 'Enter description');
        this.prioritySel = new Select('Select priority', priorityOptionsList);
        this.pressureInp = new Input('pressure', 'text', 'Enter normal pressure');
        this.indexInp = new Input('index','text', 'Enter your weight index');
        this.diseasesInp = new Input('diseases','text', 'Enter any heard diseases in past');
        this.ageInp = new Input('age', 'text', 'Enter age');
        this.lastInp = new Input('last', 'date', 'Enter lase visit date');
        this.createBtn = new FormButton('Create', 'input-button');
        this.editBtn = new FormButton('Edit','input-button');
    }
    renderEditButton() {
        this.editButton = this.editBtn.render();
        return this.editButton;
    }

    // THERAPIST
    renderTherapist(doctor, visitData = null) {
        this.therapist = document.createElement('form');
        this.therapist.classList.add('doctor-form');
        this.therName = document.createElement('h2');
        this.therName.classList.add('green-font');
        this.therName.textContent = doctor;
        this.therName.classList.add('h2-style');
        this.priority = this.prioritySel.render('Select priority', priorityOptionsList);
        this.patient = this.patientInp.render();
        this.purpose = this.purposeInp.render();
        this.description = this.descInp.render();
        this.age = this.ageInp.render();

        if(visitData) {
            this.priority.value = visitData.priority;
            this.patient.value = visitData.patient;
            this.purpose.value = visitData.purpose;
            this.description.value = visitData.description;
            this.age.value = visitData.age;
        }

        this.therapist.append(
            this.therName,
            this.priority,
            this.patient,
            this.purpose,
            this.description,
            this.age
        );
        return this.therapist;
    }
    renderCreateTherapistButton(parent) {
        this.createButton = this.createBtn.render();
        this.createButton.addEventListener('click', event => {
            event.preventDefault();
            if(this.sendTherapist()) {
                createNewVisit(this.sendTherapist())
                    .then(json => {
                        let newTherapistVisit = new VisitTherapist(json);
                        let newTherapistDOM = newTherapistVisit.render();
                        openCardsContainer.append(newTherapistDOM);
                        cardsArray.push(newTherapistVisit);
                        chooseDoctorForm.hide();
                        searchForm.searchCheck();
                    });
            }
        });
        parent.append(this.createButton);
    }
    sendTherapist() {
        const therapistResults = new FormData(this.therapist);
        const therapistBody = {
            purpose: therapistResults.get('purpose'),
            doctor: 'Therapist',
            description: therapistResults.get('desc'),
            age: therapistResults.get('age'),
            priority: this.priority.value,
            patient: therapistResults.get('patient'),
            index: '',
            diseases: '',
            last: '',
            status: 'open',
        };
        if(
            therapistBody.purpose
            && therapistBody.doctor
            && therapistBody.description
            && therapistBody.age
            && therapistBody.patient
        ) { return therapistBody;
        } else {
            this.showErrorTherapist();
            return null;
        }
    }
    showErrorTherapist() {
        if(!this.therapist.contains(this.error)) {
            this.error = document.createElement('p');
            this.error.textContent = 'You must fill all fields';
            this.error.classList.add('error-message');
            this.therapist.append(this.error);
        }
    }

    //DENTIST
    renderDentist(doctor, visitData = null) {
        this.dentist = document.createElement('form');
        this.dentist.classList.add('doctor-form');
        this.therName = document.createElement('h2');
        this.therName.classList.add('blue-font');
        this.therName.textContent = doctor;
        this.therName.classList.add('h2-style');
        this.priority = this.prioritySel.render('Select priority', priorityOptionsList);
        this.patient = this.patientInp.render();
        this.purpose = this.purposeInp.render();
        this.description = this.descInp.render();
        this.last = this.lastInp.render();

        if(visitData) {
            this.priority.value = visitData.priority;
            this.patient.value = visitData.patient;
            this.purpose.value = visitData.purpose;
            this.description.value = visitData.description;
            this.last.value = visitData.last;
        }

        this.dentist.append(
            this.therName,
            this.priority,
            this.patient,
            this.purpose,
            this.description,
            this.last
        );
        return this.dentist;
    }
    renderCreateDentistButton(parent) {
        this.createButton = this.createBtn.render();
        this.createButton.addEventListener('click', event => {
            event.preventDefault();
            if(this.sendDentist()) {
                createNewVisit(this.sendDentist())
                    .then(json => {
                        let newDentistVisit = new VisitDentist(json);
                        let newDentistDOM = newDentistVisit.render();
                        openCardsContainer.append(newDentistDOM);
                        cardsArray.push(newDentistVisit);
                        chooseDoctorForm.hide();
                        searchForm.searchCheck();
                    });
            }
        });
        parent.append(this.createButton);
    }
    sendDentist() {
        const dentistResults = new FormData(this.dentist);
        const dentistBody = {
            purpose: dentistResults.get('purpose'),
            doctor: 'Dentist',
            description: dentistResults.get('desc'),
            last: dentistResults.get('last'),
            priority: this.priority.value,
            patient: dentistResults.get('patient'),
            index: '',
            diseases: '',
            age: '',
            status: 'open',
        };
        if(
            dentistBody.purpose
            && dentistBody.doctor
            && dentistBody.description
            && dentistBody.last
            && dentistBody.patient
        ) { return dentistBody;
        } else {
            this.showErrorDentist();
            return null;
        }
    }
    showErrorDentist() {
        if(!this.dentist.contains(this.error)) {
            this.error = document.createElement('p');
            this.error.textContent = 'You must fill all fields';
            this.error.classList.add('error-message');
            this.dentist.append(this.error);
        }
    }

    //CARDIOLOGIST
    renderCardiologist(doctor, visitData = null) {
        this.cardiologist = document.createElement('form');
        this.cardiologist.classList.add('doctor-form');
        this.therName = document.createElement('h2');
        this.therName.classList.add('red-font');
        this.therName.textContent = doctor;
        this.therName.classList.add('h2-style');
        this.priority = this.prioritySel.render('Select priority', priorityOptionsList);
        this.patient = this.patientInp.render();
        this.purpose = this.purposeInp.render();
        this.description = this.descInp.render();
        this.description.classList.add('description');
        this.age = this.ageInp.render();
        this.index = this.indexInp.render();
        this.pressure = this.pressureInp.render();
        this.diseases = this.diseasesInp.render();

        if(visitData) {
            this.priority.value = visitData.priority;
            this.patient.value = visitData.patient;
            this.purpose.value = visitData.purpose;
            this.description.value = visitData.description;
            this.age.value = visitData.age;
            this.index.value = visitData.index;
            this.pressure.value = visitData.pressure;
            this.diseases.value = visitData.diseases;
        }

        this.cardiologist.append(
            this.therName,
            this.priority,
            this.patient,
            this.purpose,
            this.description,
            this.age,
            this.index,
            this.pressure,
            this.diseases
        );
        return this.cardiologist;
    }
    renderCreateCardiologistButton(parent) {
        this.createButton = this.createBtn.render();
        this.createButton.addEventListener('click', event => {
            event.preventDefault();
            if(this.sendCardiologist()) {
                createNewVisit(this.sendCardiologist())
                    .then(json => {
                        let newCardiologistVisit = new VisitCardiologist(json);
                        let newCardiologistDOM = newCardiologistVisit.render();
                        openCardsContainer.append(newCardiologistDOM);
                        cardsArray.push(newCardiologistVisit);
                        chooseDoctorForm.hide();
                        searchForm.searchCheck();
                    });
            }
        });
        parent.append(this.createButton);
    }
    sendCardiologist() {
        const cardiologistResults = new FormData(this.cardiologist);
        const cardiologistBody = {
            purpose: cardiologistResults.get('purpose'),
            doctor: 'Cardiologist',
            description: cardiologistResults.get('desc'),
            age: cardiologistResults.get('age'),
            priority: this.priority.value,
            patient: cardiologistResults.get('patient'),
            index: cardiologistResults.get('index'),
            diseases: cardiologistResults.get('diseases'),
            pressure: cardiologistResults.get('pressure'),
            last: '',
            status: 'open',
        };
        if(
            cardiologistBody.purpose
            && cardiologistBody.doctor
            && cardiologistBody.description
            && cardiologistBody.age
            && cardiologistBody.patient
            && cardiologistBody.index
            && cardiologistBody.pressure
            && cardiologistBody.diseases
        ) { return cardiologistBody;
        } else {
            this.showErrorCardiologist();
            return null;
        }
    }
    showErrorCardiologist() {
        if(!this.cardiologist.contains(this.error)) {
            this.error = document.createElement('p');
            this.error.textContent = 'You must fill all fields';
            this.error.classList.add('error-message');
            this.cardiologist.append(this.error);
        }
    }
}