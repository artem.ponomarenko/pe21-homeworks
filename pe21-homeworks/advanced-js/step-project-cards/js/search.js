class SearchForm {
    constructor() {
        this.searchInp = new Input('Search', 'text', 'Search by purpose or description');
        this.prioritySel = new Select('Select priority', [
            {name: 'Show all', value: 'all'},
            {name: 'Normal only', value: 'normal'},
            {name: 'High only', value: 'high'},
            {name: 'Urgent only', value: 'urgent'}
        ]);
        this.statusSel = new Select('Select Status', [
            {name: 'Show all', value: 'all'},
            {name: 'Open only', value: 'open'},
            {name: 'Closed only', value: 'closed'}
        ]);
    }
    render(parent) {
        this.searchPan = document.createElement('div');
        this.searchPan.classList.add('search-panel', 'hidden');

        this.name = document.createElement('h2');
        this.name.classList.add('h3-card-name', 'grey-font');
        this.name.textContent = 'Search';

        this.searchInput = this.searchInp.render();
        this.searchInput.addEventListener('input', () => {
            this.searchCards();
        });
        this.prioritySelect = this.prioritySel.render();
        this.prioritySelect.addEventListener('change', ()=> {
            this.searchCards();
        });
        this.statusSelect = this.statusSel.render();
        this.statusSelect.addEventListener('change', ()=> {
            this.searchCards();
        });
        this.searchPan.append(
            this.name,
            this.searchInput,
            this.prioritySelect,
            this.statusSelect
        );
        parent.append(this.searchPan);
    }
    searchCheck() {
        if(cardsArray.length > 0) {
            this.showPanel();
            noCards.classList.add('hidden');
        } else {
            this.hidePanel();
        }
    }
    searchCards() {
        cardsArray.forEach(item => {
            let showByInput = true,
                showByPriority = true,
                showByStatus = true;
            if(this.searchInput.value) {
                showByInput = item.purpose.includes(this.searchInput.value) || item.description.includes(this.searchInput.value);
            }
            if(this.prioritySelect.value !== 'all') {
                showByPriority = item.priority.includes(this.prioritySelect.value);
            }
            if(this.statusSelect.value !== 'all') {
                showByStatus = item.status.includes(this.statusSelect.value);
            }
            if(showByInput && showByPriority && showByStatus) {
                item.showCard();
            } else {
                item.hideCard();
            }
        });
    }
    showPanel() {
        this.searchPan.classList.remove('hidden');
    }
    hidePanel() {
        this.searchPan.classList.add('hidden');
    }
}