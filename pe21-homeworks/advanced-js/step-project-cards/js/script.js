function checkToken() {
    loginToken = sessionStorage.getItem('token');
    if (!loginToken) {
        headerButton.switchToLogin();
    } else {
        headerButton.switchToCreateCard();
        getCards()
            .then(json => {
                json.forEach(card => {
                    let cardItem,
                        cardDOM;
                    switch (card.doctor) {
                        case "Therapist": {
                            cardItem = new VisitTherapist(card);
                        }
                        break;
                        case "Dentist": {
                            cardItem = new VisitDentist(card);
                        }
                            break;
                        case "Cardiologist": {
                            cardItem = new VisitCardiologist(card);
                        }
                        break;
                    }
                    cardsArray.push(cardItem);
                    cardDOM = cardItem.render();
                    cardItem.status === 'open' ? openCardsContainer.append(cardDOM) : closedCardsContainer.append(cardDOM);
                });
                searchForm.searchCheck();
                }
            );
    }
}

const headerButton = new HeaderButton('Login', 'Create card');
document.querySelector('.header').append(headerButton.render());
let loginToken;

const loginForm = new LoginForm('Please login', 'right');
loginForm.render(document.querySelector('.forms'));

const noCards = document.querySelector('.start-message');

const priorityOptionsList = [
    {
        value : 'normal',
        name : 'Normal priority'
    },
    {
        value: 'high',
        name : 'High priority'
    },
    {
        value: 'urgent',
        name : 'Urgent priority'
    },
];

let cardsArray = [];

let formsContainer = document.querySelector('.forms');

const chooseDoctorForm = new Modal('Choose doctor', 'left');
chooseDoctorForm.render(formsContainer);

const openCardsContainer = document.querySelector('.cards-container-open');
const cardsContainer = document.querySelector('.cards-container');
const searchForm = new SearchForm();
searchForm.render(cardsContainer);

const closedCardsContainer = document.querySelector('.cards-container-closed');

checkToken();






