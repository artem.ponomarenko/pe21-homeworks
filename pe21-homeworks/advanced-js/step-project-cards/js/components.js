class Input {
    constructor(name, type, placeholder) {
        this.name = name;
        this.type = type;
        this.placeholder = placeholder;
    }
    render() {
        this.input = document.createElement('input');
        this.input.type = this.type;
        this.input.name = this.name;
        this.input.placeholder = this.placeholder;
        this.input.classList.add('input-field');
        return this.input;
    }
}

class TextArea {
    constructor(name, placeholder) {
        this.name = name;
        this.placeholder = placeholder;
    }
    render() {
        this.textArea = document.createElement('textarea');
        this.textArea.classList.add('input-field');
        this.textArea.name = this.name;
        this.textArea.placeholder = this.placeholder;
        this.textArea.rows = 3;
        this.textArea.maxLength = 100;
        return this.textArea;
    }
}

class FormButton {
    constructor(name, style) {
        this.name = name;
        this.style = style;
    }
    render() {
        this.button = document.createElement('button');
        this.button.textContent = this.name;
        this.button.classList.add(this.style);
        return this.button;
    }
}

class Select {
    constructor(name, options = []) {
        this.name = name;
        this.options = options;
    }
    render() {
        this.select = document.createElement('select');
        this.select.classList.add('select-css');
        this.options.forEach((option, index) => {
            this.option = document.createElement('option');
            this.option.value = option.value;
            this.option.textContent = option.name;
            this.select.appendChild(this.option);
        });
        return this.select;
    }
}

class HeaderButton {
    constructor(login, createCard) {
        this.login = login;
        this.createCard = createCard;
    }
    render() {
        this.hButton = document.createElement('button');
        this.hButton.innerText = this.login;
        this.hButton.classList.add('header-button');
        this.hButton.addEventListener('click', ()=> {
            if(this.hButton.innerText === this.login) {
                loginForm.show();
            } else if (this.hButton.innerText === this.createCard) {
                chooseDoctorForm.showNewModal();
            }
        });
        return this.hButton;
    }
    switchToLogin() {
        this.hButton.classList.add('red-bg');
        this.hButton.innerText = this.login;
    }
    switchToCreateCard() {
        this.hButton.classList.remove('red-bg');
        this.hButton.innerText = this.createCard;
    }
}