'use strict';
/*
Как я понял, бросание ошибок будет полезно при обработке данных, и их отображении.
Если код отслеживает какие-то ошибки или недостаточные данные, можно сообщить пользователю/разработчику, где именно
и какого характера вылезла ошибка и упростить поиск и исправление.
 */

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const ulList = document.createElement('ul');
const ulWrapper = document.getElementById('root');

let createListItem = function (author, name, price) {
  let listItem = document.createElement('li');
  listItem.innerHTML = `Автор: ${author}, <br>Название: ${name}, <br><b>Цена: ${price} грн</b>`;
  return listItem;
};

books.forEach(item => {
    let {author, name, price} = item;
    try {
        if(author && name && price) {
            ulList.append(createListItem(author, name, price));
        } else if(!author) {
            throw new Error(`Кинга: ${name} не имеет АВТОРА`);
        } else if(!name) {
            throw new Error(`Книга не имеет НАЗВАНИЯ`);
        } else if(!price) {
            throw new Error(`Книга: ${name} не имеет ЦЕНЫ`);
        }
    } catch (e) {
        console.error(e);
    }
});

ulWrapper.append(ulList);

