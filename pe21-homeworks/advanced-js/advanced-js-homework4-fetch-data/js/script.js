/*
AJAX - это набор javascript технологий фоновой обработки запросов, позволяющий получать от сервера данные и
 отображать их на странице без полной перезагрузки страницы. Что и как отображать контролирует js код.
  Он отправляет запросы и обрабатывает данные не замораживая работу пользователя на странице.
  Эти технологи позволяют создавать быстрые и удобные для пользователя приложения.


  Пробовал прилепить прелоадер, но чето не получилось нормально это сделать)
  Код коряво написан, видимо) Я очень путаюсь с этими зенами)
 */


const filmsURL = 'https://swapi.dev/api/films/';
const filmsList = document.getElementById('filmsList');

async function renderCharacter(characterLink, parent) {
    let response = await fetch(characterLink);
    let characterInfo = await response.json();
    let characterDOM = new characterCard(characterInfo);
    characterDOM.render(parent);
}

class filmDOM {
    constructor({title, opening_crawl, episode_id}) {
        this.title = title;
        this.openingCrawl = opening_crawl;
        this.episode = episode_id;
    }
    render() {
        this.filmContainer = document.createElement('div');
        this.filmTitle = document.createElement('h3');
        this.filmEpisode = document.createElement('p');
        this.filmDescription = document.createElement('p');

        this.filmContainer.classList.add('film-container');
        this.filmTitle.textContent = this.title;
        this.filmDescription.textContent = this.openingCrawl;
        this.filmEpisode.textContent = 'Episode ' + this.episode;

        this.filmContainer.append(this.filmTitle, this.filmEpisode, this.filmDescription);
        filmsList.append(this.filmContainer);
    }
}

class characterCard {
    constructor({name}) {
        this.character = name;
    }
    render(parent) {
        this.characterName = document.createElement('span');
        this.characterName.textContent = this.character;
        this.characterName.classList.add('character-name');
        parent.append(this.characterName);
    }

}

fetch(filmsURL)
    .then(response => {
        if(response.ok) {
            return response.json();
        } else {
            throw new Error('Error loading info');
        }
    })
    .then(json => {
        const filmsCards = json.results.map(film => new filmDOM(film));
        filmsCards.forEach(item => {
            item.render();
                });
        return json.results;
    })
    .then(films => {
        films.forEach((item, index) => {

            let charactersList = document.createElement('div');
            let charactersListName = document.createElement('h4');
            charactersListName.textContent = 'Film characters:';
            charactersList.append(charactersListName);

            item.characters.forEach(item => renderCharacter(item, charactersList));
            filmsList.children[index].append(charactersList);
        })
    })
    .catch(error => console.error(error));
