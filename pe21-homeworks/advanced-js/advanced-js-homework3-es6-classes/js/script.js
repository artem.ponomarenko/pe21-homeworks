'use strict';
/*
Прототипное наследование позволяет создавать конструкторы объектов, с различными методами и свойствами,
и в последствии ипсользовать их как "шаблоны" для быстрого создания новых элеметов, которые будут иметь
сразу все необходимые свойства определенные в шаблоне.
Также позволяет реализовать создание подчиненных шаблонов, с расширенными или переопределенными свойствами/методами.
В итоге все это позволяет удобно и быстро создавать новые элементы из уже определенных шаблонов.
 */

class Employee {
    constructor(name,age,salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get getName() {
        return this.name;
    }
    get getAge() {
        return this.age;
    }
    get getSalary() {
        return this.salary;
    }
    set setName(newName) {
        return this.name = newName;
    }
    set setAge(newAge) {
        return this.age = newAge;
    }
    set setSalary(newSalary) {
        return this.salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, languages) {
        super(name, age, salary);
        this.lang = languages;
    }
    get getSalary() {
        return this.salary * 3;
    }
}

const employee1 = new Employee('Sasha', 45, 1500);
console.log(employee1);

const programmer1 = new Programmer('Petya', 20, 1000, ['english, spanish, german']);
const programmer2 = new Programmer('Dima', 22, 1500, ['english, german']);
const programmer3 = new Programmer('Vitya', 28, 1000, ['russian, italian, german']);
console.log(programmer1,programmer2,programmer3);