'use strict';

const feedbacks = [
    {
        name: 'Arnold Shvatzenegger',
        job: 'UX Designer',
        image: './img/people-say/people/arnold-shvarzenegger.png',
        text: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
    },
    {
        name: 'Brad Pitt',
        job: 'Senior Developer',
        image: './img/people-say/people/brad-pitt.png',
        text: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
    },
    {
        name: 'Clint Eastwood',
        job: 'Frontend Developer',
        image: './img/people-say/people/clint-eastwood.png',
        text: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
    },
    {
        name: 'Johny Depp',
        job: 'UX Designer',
        image: './img/people-say/people/johny-depp.png',
        text: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
    },
    {
        name: 'Leonardo Dicaprio',
        job: 'Junior Developer',
        image: './img/people-say/people/leonardo-dicaprio.png',
        text: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
    },
    {
        name: 'Mila Kunis',
        job: 'Team Lead',
        image: './img/people-say/people/mila-kunis.png',
        text: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
    },
    {
        name: 'morgan-freeman',
        job: 'Web Master',
        image: './img/people-say/people/morgan-freeman.png',
        text: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
    },
    {
        name: 'Tom Hanks',
        job: 'CEO',
        image: './img/people-say/people/tom-hanks.png',
        text: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.',
    }
];

// FUNCTIONS

function showFeedbackIcons() {
    for (let i = 0; i < iconsToShowSet; i++) {
        addNewIconRight(i);
        addNewFeedbackRight(i);
    }
    activeIcon = iconsContainer.children[activeIconIndex];
    activeIcon.classList.add('active-icon');
}

function setNewActive(index) {
    activeIcon.classList.remove('active-icon');
    activeIcon = iconsContainer.children[index];
    activeIcon.classList.add('active-icon');
    feedbacksContainer.style.left = `-${index*1160}px`;
}

function addNewFeedbackRight(index) {
    let newFeedback = document.createElement('div');
    newFeedback.classList.add('feedback');
    newFeedback.innerHTML = `
<div class="feedback-wrapper">
    <p class="feedback-text">${feedbacks[index].text}</p>
                <span class="feedback-name">${feedbacks[index].name}</span>
                <span class="feedback-position">${feedbacks[index].job}</span>
                <img src="${feedbacks[index].image}" alt="${feedbacks[index].name}" class="feedback-image">
</div>
    `;
    feedbacksContainer.append(newFeedback);
}

function addNewFeedbackLeft(index) {
    let newFeedback = document.createElement('div');
    newFeedback.classList.add('feedback');
    newFeedback.classList.add('feedback-hidden');
    newFeedback.innerHTML = `
<div class="feedback-wrapper">
    <p class="feedback-text">${feedbacks[index].text}</p>
                <span class="feedback-name">${feedbacks[index].name}</span>
                <span class="feedback-position">${feedbacks[index].job}</span>
                <img src="${feedbacks[index].image}" alt="${feedbacks[index].name}" class="feedback-image">
</div>
    `;
    feedbacksContainer.prepend(newFeedback);

    setTimeout( function() {
        newFeedback.classList.remove('feedback-hidden');
    }, 100);
}

function addNewIconRight(index) {
    let newIcon = document.createElement('div');
    newIcon.classList.add('feedback-icon-wrapper');
    newIcon.innerHTML = `<img src="${feedbacks[index].image}" alt="${feedbacks[index].name}" class="feedback-icon">`;
    iconsContainer.append(newIcon);
}

function addNewIconLeft(index) {
    let newIcon = document.createElement('div');
    newIcon.classList.add('feedback-icon-wrapper');
    newIcon.classList.add('icon-hidden');
    newIcon.innerHTML = `<img src="${feedbacks[index].image}" alt="${feedbacks[index].name}" class="feedback-icon">`;
    iconsContainer.prepend(newIcon);

    setTimeout( function() {
        newIcon.classList.remove('icon-hidden');
    }, 100);
}

function removeLeftIcon() {
    let itemToRemove = iconsContainer.children[0];
    itemToRemove.classList.add('icon-hidden');
    itemToRemove.addEventListener('transitionend', function () {
        itemToRemove.remove();
    });
}

function removeLeftFeedback() {
    let feedbackToRemove = feedbacksContainer.children[0];
    feedbackToRemove.classList.add('feedback-hidden');
    feedbackToRemove.addEventListener('transitionend', function () {
        feedbacksContainer.children[0].remove();
    });
}

function removeRightIcon() {
    setTimeout( function() {
        iconsContainer.lastElementChild.remove();
    }, 300);
}

function removeRightFeedback() {
    setTimeout( function() {
        console.log(feedbacksContainer.lastElementChild);
        feedbacksContainer.lastElementChild.remove();
    }, 300);
}


// BODY
const iconsContainer = document.getElementById('feedbackIconsContainer'),
    feedbacksContainer = document.getElementById('feedbacksContainer'),
    arrowRight = document.getElementById('feedbackArrowRight'),
    arrowLeft = document.getElementById('feedbackArrowLeft'),
    iconsToShowSet = 5;

let activeIcon,
    activeIconIndex = 0,
    indexToAddRight = iconsToShowSet - 1,
    indexToAddLeft = 0;

showFeedbackIcons();

iconsContainer.addEventListener('click', event => {
    let current = event.target;
    if (current.nodeName === 'IMG' && current.parentNode !== activeIcon) {
        activeIconIndex = [...current.parentNode.parentNode.children].indexOf(current.parentNode);
        setNewActive(activeIconIndex);
    }
});

arrowRight.addEventListener('click', ()=> {

    if (activeIconIndex < iconsToShowSet - 1) {
        activeIconIndex++;
        setNewActive(activeIconIndex);

    } else {
        indexToAddRight++;
        indexToAddLeft = indexToAddRight - iconsToShowSet;
        if (indexToAddRight >= feedbacks.length) {
            indexToAddRight = 0;
            indexToAddLeft = feedbacks.length - (iconsToShowSet - 1);
        }

        addNewIconRight(indexToAddRight);
        addNewFeedbackRight(indexToAddRight);
        removeLeftIcon();
        removeLeftFeedback();
        setTimeout(()=> setNewActive(activeIconIndex), 250);
    }
});

arrowLeft.addEventListener('click', ()=> {

    if (activeIconIndex > 0) {
        activeIconIndex--;
        setNewActive(activeIconIndex);
    } else {
        indexToAddLeft--;
        indexToAddRight = indexToAddLeft + iconsToShowSet;
        if (indexToAddLeft < 0) {
            indexToAddLeft = feedbacks.length - 1;
            indexToAddRight = iconsToShowSet - 2;
        }

        addNewIconLeft(indexToAddLeft);
        addNewFeedbackLeft(indexToAddLeft);
        removeRightIcon();
        removeRightFeedback();
        setTimeout(()=> setNewActive(activeIconIndex), 250);
    }
});