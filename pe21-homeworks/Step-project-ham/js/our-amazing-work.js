'use strict';

const ourWorks = [
    {
        category: 'Graphic Design',
        name: 'Creative Design',
        image: './img/our-amazing-work/graphic-design/graphic-design1.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Web Design',
        name: 'Web Shop',
        image: './img/our-amazing-work/web-design/web-design3.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Graphic Design',
        name: 'Great Name Cards',
        image: './img/our-amazing-work/graphic-design/graphic-design3.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Graphic Design',
        name: 'Nice Work',
        image: './img/our-amazing-work/graphic-design/graphic-design5.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Graphic Design',
        name: 'Best Work Ever',
        image: './img/our-amazing-work/graphic-design/graphic-design6.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Graphic Design',
        name: 'Cool Graphic',
        image: './img/our-amazing-work/graphic-design/graphic-design7.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Web Design',
        name: 'Model Agency',
        image: './img/our-amazing-work/web-design/web-design5.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Graphic Design',
        name: 'Best Emotions',
        image: './img/our-amazing-work/graphic-design/graphic-design9.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Web Design',
        name: 'Cool Web Site',
        image: './img/our-amazing-work/web-design/web-design2.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Graphic Design',
        name: 'Nice Catalogue',
        image: './img/our-amazing-work/graphic-design/graphic-design10.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Graphic Design',
        name: 'Best Catalogue',
        image: './img/our-amazing-work/graphic-design/graphic-design11.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Graphic Design',
        name: 'Great Logo',
        image: './img/our-amazing-work/graphic-design/graphic-design12.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Web Design',
        name: 'Cool Web',
        image: './img/our-amazing-work/web-design/web-design1.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Wordpress',
        name: 'Metal Store',
        image: './img/our-amazing-work/wordpress/wordpress9.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Web Design',
        name: 'Web Site Sertiz',
        image: './img/our-amazing-work/web-design/web-design4.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Graphic Design',
        name: 'Awesome Design',
        image: './img/our-amazing-work/graphic-design/graphic-design2.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Landing Pages',
        name: 'Great Landing',
        image: './img/our-amazing-work/landing-page/landing-page4.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Web Design',
        name: 'Web Store',
        image: './img/our-amazing-work/web-design/web-design7.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Landing Pages',
        name: 'New Landing',
        image: './img/our-amazing-work/landing-page/landing-page1.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Landing Pages',
        name: 'Best Landing',
        image: './img/our-amazing-work/landing-page/landing-page2.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Wordpress',
        name: 'New Store',
        image: './img/our-amazing-work/wordpress/wordpress2.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Landing Pages',
        name: 'Awesome Landing',
        image: './img/our-amazing-work/landing-page/landing-page3.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Landing Pages',
        name: 'Great Landing',
        image: './img/our-amazing-work/landing-page/landing-page4.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Landing Pages',
        name: 'New Work',
        image: './img/our-amazing-work/landing-page/landing-page5.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Landing Pages',
        name: 'Shoe Company',
        image: './img/our-amazing-work/landing-page/landing-page6.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Graphic Design',
        name: 'Cool Graphic',
        image: './img/our-amazing-work/graphic-design/graphic-design4.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Landing Pages',
        name: 'Game Company',
        image: './img/our-amazing-work/landing-page/landing-page7.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Wordpress',
        name: 'Wordpress Store',
        image: './img/our-amazing-work/wordpress/wordpress1.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Wordpress',
        name: 'Awesome Store',
        image: './img/our-amazing-work/wordpress/wordpress3.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Wordpress',
        name: 'Car Store',
        image: './img/our-amazing-work/wordpress/wordpress4.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Graphic Design',
        name: 'Best Booklets',
        image: './img/our-amazing-work/graphic-design/graphic-design8.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Wordpress',
        name: 'Wood Store',
        image: './img/our-amazing-work/wordpress/wordpress5.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Wordpress',
        name: 'Furniture Store',
        image: './img/our-amazing-work/wordpress/wordpress6.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Wordpress',
        name: 'Stone Store',
        image: './img/our-amazing-work/wordpress/wordpress7.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Web Design',
        name: 'Motor Web Site',
        image: './img/our-amazing-work/web-design/web-design6.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Wordpress',
        name: 'Toy Store',
        image: './img/our-amazing-work/wordpress/wordpress8.jpg',
        linkLeft: '#',
        linkRight: '#'
    },
    {
        category: 'Wordpress',
        name: 'Meal Store',
        image: './img/our-amazing-work/wordpress/wordpress10.jpg',
        linkLeft: '#',
        linkRight: '#'
    }
];

// Select categories from ourWorks array
Array.prototype.selectUniqueCategories = function() {
    let arrayOfCategories = [];
    for (let i = 0; i<this.length; i++) {
        if (!arrayOfCategories.includes(this[i].category)) {
            arrayOfCategories.push(this[i].category);
        }
    }
    return arrayOfCategories;
};


// Select items of chosen category
Array.prototype.selectCategoryItems = function(category) {
    let ourCategoryWorks = [];
    ourWorks.forEach((item) => {
        if (item.category === category.textContent) {
            ourCategoryWorks.push(item);
        }
    });
    return ourCategoryWorks;
};


function showActiveWorkCategory() {
    itemShown = 0;
    iterationsCounter = 0;
    stopPoint = 0;

    if (activeCategory.textContent === 'All') {
        showWorkItems(ourWorks);
    } else {
        showWorkItems(ourWorks.selectCategoryItems(activeCategory));
    }
    categories.after(worksList);
}


function showWorkItems(works) {

    itemShown + itemsToShowOnceSet < works.length ? stopPoint = itemShown + itemsToShowOnceSet : stopPoint = works.length;

    for (itemShown; itemShown < stopPoint; itemShown++) {

        let newWorkItem = document.createElement('div');
        newWorkItem.classList.add('work-item');

        newWorkItem.innerHTML = `
        <img src="${works[itemShown].image}" alt="${works[itemShown].name}" class="work-image">
                <div class="work-item-content">
                    <div class="work-links">
                        <a href="${works[itemShown].linkLeft}" class="work-link-left"></a>
                        <a href="${works[itemShown].linkRight}" class="work-link-right"></a>
                    </div>
                    <span class="work-name">${works[itemShown].name}</span>
                    <span class="work-category">${works[itemShown].category}</span>
                </div>
        `;
        worksList.append(newWorkItem);
    }
    iterationsCounter++;

    if (itemShown < works.length && iterationsCounter < iterationSet) {
        loadMoreButton.style.display = 'flex';
        loadMoreButton.onclick = function() { showWorkItems(works) };
    } else {
        loadMoreButton.style.display = 'none';
    }
}


// CODE BODY

const itemsToShowOnceSet = 12,
    iterationSet = 3,
    uniqueWorkCategories = ourWorks.selectUniqueCategories(),
    categories = document.getElementById('worksCategories'),
    worksList = document.createElement('div'),
    loadMoreButton = document.getElementById('loadMore');

let itemShown = 0,
    iterationsCounter = 0,
    stopPoint = 0,
    activeCategory = categories.children[0];

worksList.classList.add('works-items');
loadMoreButton.style.display = 'none';

uniqueWorkCategories.forEach((item, index) => {
    let newCategory = document.createElement('li');
    newCategory.classList.add('works-category');
    newCategory.textContent = item;
    categories.append(newCategory);
});

showActiveWorkCategory();

categories.addEventListener('click', event => {
    let currentCategory = event.target;

    if (currentCategory !== activeCategory) {
        activeCategory.classList.remove('active-category');
        currentCategory.classList.add('active-category');
        worksList.remove();
        worksList.innerHTML = '';
        activeCategory = currentCategory;

        showActiveWorkCategory();
    }
});
