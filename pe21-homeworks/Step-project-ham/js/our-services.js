'use strict';

const ourServices = [
    {
        serviceName: 'Web Design',
        serviceImageUrl: './img/services/web-design.png',
        serviceText: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aut consectetur delectus' +
            ' modi nesciunt omnis quas quos recusandae temporibus voluptatibus. Adipisci blanditiis commodi' +
            ' consequuntur cupiditate delectus deleniti dignissimos doloremque eaque eligendi est et eum fuga id inventore itaque iusto labore, libero, molestias, nihil nostrum obcaecati odio omnis perspiciatis porro quas quasi quidem quisquam quo quod quos repellat repellendus sequi sit soluta totam velit voluptas? Amet, asperiores at debitis dolor enim error neque provident quae, rem sapiente sunt, velit voluptatem. Soluta.',
    },
    {
        serviceName: 'Graphic Design',
        serviceImageUrl: './img/services/graphic-design.png',
        serviceText: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aut consectetur delectus' +
            ' modi nesciunt omnis quas quos recusandae temporibus voluptatibus. Adipisci blanditiis commodi' +
            ' consequuntur cupiditate delectus deleniti dignissimos doloremque eaque eligendi est et eum fuga id inventore itaque iusto labore, libero, molestias, nihil nostrum obcaecati odio omnis perspiciatis porro quas quasi quidem quisquam quo quod quos repellat repellendus sequi sit soluta totam velit voluptas? Amet, asperiores at debitis dolor enim error neque provident quae, rem sapiente sunt, velit voluptatem. Soluta.',
    },
    {
        serviceName: 'Online Support',
        serviceImageUrl: './img/services/online-support.png',
        serviceText: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aut consectetur delectus' +
            ' modi nesciunt omnis quas quos recusandae temporibus voluptatibus. Adipisci blanditiis commodi' +
            ' consequuntur cupiditate delectus deleniti dignissimos doloremque eaque eligendi est et eum fuga id inventore itaque iusto labore, libero, molestias, nihil nostrum obcaecati odio omnis perspiciatis porro quas quasi quidem quisquam quo quod quos repellat repellendus sequi sit soluta totam velit voluptas? Amet, asperiores at debitis dolor enim error neque provident quae, rem sapiente sunt, velit voluptatem. Soluta.',
    },
    {
        serviceName: 'App Design',
        serviceImageUrl: './img/services/app-design.png',
        serviceText: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aut consectetur delectus' +
            ' modi nesciunt omnis quas quos recusandae temporibus voluptatibus. Adipisci blanditiis commodi' +
            ' consequuntur cupiditate delectus deleniti dignissimos doloremque eaque eligendi est et eum fuga id inventore itaque iusto labore, libero, molestias, nihil nostrum obcaecati odio omnis perspiciatis porro quas quasi quidem quisquam quo quod quos repellat repellendus sequi sit soluta totam velit voluptas? Amet, asperiores at debitis dolor enim error neque provident quae, rem sapiente sunt, velit voluptatem. Soluta.',
    },
    {
        serviceName: 'Online Marketing',
        serviceImageUrl: './img/services/online-marketing.png',
        serviceText: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aut consectetur delectus' +
            ' modi nesciunt omnis quas quos recusandae temporibus voluptatibus. Adipisci blanditiis commodi' +
            ' consequuntur cupiditate delectus deleniti dignissimos doloremque eaque eligendi est et eum fuga id inventore itaque iusto labore, libero, molestias, nihil nostrum obcaecati odio omnis perspiciatis porro quas quasi quidem quisquam quo quod quos repellat repellendus sequi sit soluta totam velit voluptas? Amet, asperiores at debitis dolor enim error neque provident quae, rem sapiente sunt, velit voluptatem. Soluta.',
    },
    {
        serviceName: 'Seo Service',
        serviceImageUrl: './img/services/seo-services.png',
        serviceText: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aut consectetur delectus' +
            ' modi nesciunt omnis quas quos recusandae temporibus voluptatibus. Adipisci blanditiis commodi' +
            ' consequuntur cupiditate delectus deleniti dignissimos doloremque eaque eligendi est et eum fuga id inventore itaque iusto labore, libero, molestias, nihil nostrum obcaecati odio omnis perspiciatis porro quas quasi quidem quisquam quo quod quos repellat repellendus sequi sit soluta totam velit voluptas? Amet, asperiores at debitis dolor enim error neque provident quae, rem sapiente sunt, velit voluptatem. Soluta.',
    },
];

let servicesSelection = document.getElementById('servicesList'),
    servicesArray = [...servicesSelection.children],
    activeServiceIndex,
    activeServiceItem;

function showActiveService() {

    activeServiceItem = servicesArray.find((item, index) => {
        if (item.classList.contains('active-service')) {
            activeServiceIndex = index;
            return item;
        }
    });

    let activeServiceContent = document.getElementById('servicesText');
    activeServiceContent.children[0].src = ourServices[activeServiceIndex].serviceImageUrl;
    activeServiceContent.children[0].alt = ourServices[activeServiceIndex].serviceName;
    activeServiceContent.children[1].textContent = ourServices[activeServiceIndex].serviceText;
}

showActiveService();

servicesSelection.addEventListener('click',event => {
    let currentService = event.target;
    if (currentService !== activeServiceItem) {
        activeServiceItem.classList.remove('active-service');
        currentService.classList.add('active-service');
        showActiveService();
    }
});