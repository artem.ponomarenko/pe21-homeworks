const gulp = require('gulp'),
    clean = require('gulp-clean'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    sass = require('gulp-sass'),
    cleanCSS = require('gulp-clean-css'),
    minifyjs = require('gulp-js-minify'),
    uglify = require('gulp-uglify'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create();


const paths = {
    src: {
        styles: 'src/scss/**/*.scss',
        js: 'src/js/**/*.js',
        img: 'src/img/**/*.+(jpg|png|svg|jpeg|gif)',
        html: '*.html'
    },
    build: {
        styles: 'dist/css/',
        js: 'dist/js/',
        img: 'dist/img/',
        html: 'dist/'
    }
};

const moveHTMLFiles = () => (
    gulp.src(paths.src.html)
        .pipe(gulp.dest(paths.build.html))
);

const cleanBuild = () => (
    gulp.src(paths.build.html, {allowEmpty: true})
        .pipe(clean())
);

const  buildJS = ()=> (
    gulp.src(paths.src.js)
        .pipe(concat('script.min.js'))
        .pipe(minifyjs())
        .pipe(uglify())
        .pipe(gulp.dest(paths.build.js))
);

const  buildCSS = ()=> (
    gulp.src(paths.src.styles)
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.min.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(autoprefixer({
            browsers: ['> 0.5%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1'],
            cascade: false
        }))
        .pipe(gulp.dest(paths.build.styles))
);

const imageMin = ()=> (
    gulp.src(paths.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.build.img))
);

const watcher = ()=> {
    browserSync.init({
        server: {
            baseDir: './dist'
        }
    });
    gulp.watch(paths.src.html, moveHTMLFiles).on('change',browserSync.reload);
    gulp.watch(paths.src.js, buildJS).on('change',browserSync.reload);
    gulp.watch(paths.src.styles, buildCSS).on('change',browserSync.reload);
    gulp.watch(paths.src.img, imageMin).on('change',browserSync.reload);
};

gulp.task('dev', gulp.series(
    cleanBuild,
    moveHTMLFiles,
    buildJS,
    buildCSS,
    imageMin,
    watcher
));

gulp.task('build', gulp.series(
    cleanBuild,
    moveHTMLFiles,
    buildJS,
    buildCSS,
    imageMin,
));