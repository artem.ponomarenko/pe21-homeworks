const postsElements = document.getElementById("postsBlock"),
    instaElements = document.getElementById("instaContainer"),
    menuButton = document.getElementById("navbarButton"),
    menuWrapper = document.getElementById("menuWrapper");

let postElement, instaElement;

function showPosts(quantity) {
    for (let i = posts.length - 1; i >= posts.length - quantity; i--) {
        postElement = document.createElement("div");
        postElement.classList.add("block__item");
        postElement.innerHTML = `<a href="${posts[i].link}" class="block__item-link">
                    <img class="block__item-image" src="${posts[i].imageUrl}" alt="${posts[i].name}">
                </a>
                <div class="block__item-content">
                    <h3 class="block__item-name">${posts[i].name}</h3>
                    <div class="block__item-specs">
                        <a href="${posts[i].link}" class="block__item-link"><span class="block__item-span">${posts[i].category}</span>
                        </a>
                        <span class="block__item-span"> / </span>
                        <a href="${posts[i].link}" class="block__item-link"><span class="block__item-span">${posts[i].comments} Comments</span>
                        </a>
                    </div>
                    <p class="block__item-desc">${posts[i].text}</p>`;
        postsElements.append(postElement);
    }
}

function showInstaPictures(quantity) {
    for (let i = instaPosts.length-1; i >= instaPosts.length - quantity; i--) {
        instaElement = document.createElement('a');
        instaElement.classList.add('insta-link');
        instaElement.href = instaPosts[i].link;
        instaElement.innerHTML = `<img src="${instaPosts[i].postImageURL}" alt="Instagram image" class="insta-image">`;
        instaElements.append(instaElement);
    }
}

showPosts(4);
showInstaPictures(4);

menuButton.addEventListener('click', ()=> {
    menuButton.classList.toggle('is-active');
    menuWrapper.classList.toggle('active-menu');
});
