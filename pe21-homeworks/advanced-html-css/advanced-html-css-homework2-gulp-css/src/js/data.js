const posts = [
    {
        name: "Aenean Adipiscing Etiam Vestibulum",
        category: "Photography, Journal",
        comments: 8,
        text: "Etiam porta sem malesuada euismod. Aenean leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.",
        imageUrl: "./img/posts/post4.jpg",
        link: "#"
    },
    {
        name: "Aenean Adipiscing Etiam Vestibulum",
        category: "Photography, Journal",
        comments: 4,
        text: "Etiam porta sem malesuada euismod. Aenean leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.",
        imageUrl: "./img/posts/post3.jpg",
        link: "#"
    },
    {
        name: "Aenean Adipiscing Etiam Vestibulum",
        category: "Photography, Journal",
        comments: 5,
        text: "Etiam porta sem malesuada euismod. Aenean leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.",
        imageUrl: "img/posts/post2.jpg",
        link: "#"
    },
    {
        name: "Aenean Adipiscing Etiam Vestibulum",
        category: "Photography, Journal",
        comments: 7,
        text: "Etiam porta sem malesuada euismod. Aenean leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.",
        imageUrl: "img/posts/post1.jpg",
        link: "#"
    },
];

const instaPosts = [
    {
        postImageURL: "./img/insta/insta4.jpg",
        link: "#"
    },
    {
        postImageURL: "./img/insta/insta3.jpg",
        link: "#"
    },
    {
        postImageURL: "./img/insta/insta2.jpg",
        link: "#"
    },
    {
        postImageURL: "./img/insta/insta1.jpg",
        link: "#"
    }
];