'use strict';

// Создать объект студент "студент" и проанализировать его табель. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
//     Создать пустой объект student, с полями name и last name.
//     Спросить у пользователя имя и фамилию студента, полученные значения записать в соответствующие поля объекта.
//     В цикле спрашивать у пользователя название предмета и оценку по нему. Если пользователь нажмет Cancel при n-вопросе о названии предмета, закончить цикл. Записать оценки по всем предметам в свойство студента tabel.
//     Посчитать количество плохих (меньше 4) оценок по предметам. Если таких нет, вывести сообщение Студент переведен на следующий курс.
//     Посчитать средний балл по предметам. Если он больше 7 - вывести сообщение Студенту назначена стипендия.

const student = {

    name: prompt("Enter Name"),
    lastName: prompt("Enter Last Name"),
    tabel: {},

    get averageMark() {
        let totalMark = 0,
            numberOfMarks = 0;
        for (let subj in student.tabel) {
            totalMark += student.tabel[subj];
            numberOfMarks++;
        }
        return (totalMark/numberOfMarks);
    },

    get badMarks() {
      let numberOfBadMarks = 0;
        for (let subj in this.tabel) {
            if (this.tabel[subj] < 4) {
                numberOfBadMarks++;
            }
        }
        return numberOfBadMarks;
    },
};

while(true) {
    let newSubject = prompt("Enter subject");
    if (!newSubject) {
        break;
    } else {
        let currentMark = +prompt("Enter mark");
        while(isNaN(currentMark) || currentMark < 1 || currentMark > 12) {
            currentMark = +prompt("Wrong entry. Try again", currentMark);
        }
        student.tabel[newSubject] = currentMark;
    }
}

if(!student.badMarks) {
    console.log("Студент переведен на следующий курс.");
} else {
    console.log("Студент провалил сессию");
}

if(student.averageMark > 7) {
    console.log("Студенту назначена стипендия");
}


