/*
Про DOM, если своими словами, то для меня - это большой объект, который содержит весь контент и разметку страницы,
 распределенную по вложенным объектам. Кроме того, у этого объекта существует множество встроенных методов для
  работы с данными на странице. DOM - "живой объект". Его содержимое динамически изменяется скриптами, и он
   обновляется при каждом изменении.
 */

'use strict';

const sourceArray =  [
    'hello',
    'world',
    'Kyiv',
    ['1', '2', '3', 'sea', 'user', ['1', '2'], 23],
    'Kharkiv',
    'Odessa',
    ['1', '2'],
    'Lviv'
];

/*
Не знаю верно ли сделал, но я не нашел другого решения с использованием MAP.
Я массив переконвертировал в строку и ее записал прямо в DIV.
Через forEach можно было бы сделать правильней, наверное, добавляя каждый элемент по отдельности.
Но в задании было создать, а потом записать.

Не уверен, что это лучшее решение, но работает)
Пришлось попотеть, конечно. Сначала вообще не мог понять, что делать. Но задание очень полезное получилось)
 */

function createList(arrayOfItems) {

    let listOfItems = arrayOfItems.map(item => {

        if (Array.isArray(item)) {
            return createList(item);
        } else {
            return `<li>${item}</li>`;
        }
    });

    return `<ul>${listOfItems.join('\n')}</ul>`;
}

function countdown() {
    let time = 10,
        timer = setInterval(frame, 1000);

    divCounter.textContent = `${time}`;

    function frame() {
        if (time === 1) {
            clearInterval(timer);
            document.write('Ups! All gone :)');
        } else {
            time--;
            divCounter.textContent = `${time}`;
        }
    }
}

let divElement = document.getElementById('list-container');
divElement.innerHTML = createList(sourceArray);

let divCounter = document.createElement('p');
divCounter.className = "counter";

divElement.after(divCounter);

countdown();





