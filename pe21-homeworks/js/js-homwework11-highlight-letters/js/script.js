'use strict';

const buttonsArray = [...document.getElementById('buttons').children];
let keypressed;

document.addEventListener('keyup', event => {

    buttonsArray.find(item => {
        if (event.key.toLowerCase() === item.textContent.toLowerCase()) {

            if (keypressed) {
                keypressed.style.backgroundColor = null;
            }
            
            item.style.backgroundColor = 'blue';
            keypressed = item;
        }
    });

});