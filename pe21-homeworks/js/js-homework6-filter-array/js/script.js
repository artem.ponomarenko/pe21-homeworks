
/*
Цикл forEach последовательно перебирает элементы нумерованного массива от 0 до последнего элемента,
и для каждой итерации (элемента массива), выполняет колбек-фукнцию, переданную ему как аргумент.
При этом он не изменяет исходный массив.
Результаты перебора зависят от, содержания колбек-функции.
 */

'use strict';

function filterArray(array, type) {
    let checkType = function(item) {
        return typeof(item) !== type ? true : false;
    };
    return array.filter(checkType);
}


let sourceArray = ['hello', 'world', undefined, {name: "Nikita", age: 20}, 23, '23', null, true, false, 46, 540985n];
console.log(sourceArray);
let type = prompt("Enter type, you want to filter out", "string");
console.log(filterArray(sourceArray,type));