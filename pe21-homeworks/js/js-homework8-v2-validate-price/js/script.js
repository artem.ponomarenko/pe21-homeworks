/*
Обработчик событий - я понимаю как метод, который мы пожем прикрутить
к любому элементу страницы, который запускается при наступлении события,
которое в него заложено. Он ждет событие (действие пользователя или
системное событие на странице), и, при его наступлении, запускает переданную
ему функцию, которая обрабатывает это событие.
 */

'use strict';

function showInput(input) {

    if (priceInputForm.contains(errorShower)) {
        errorShower.remove();
    }
    inputFormInput.style.border = null;
    inputFormInput.style.color = 'green';

    // у меня небольшое затруднение.
    // Не могу понять почему не работает тег strong.
    inputShowerText.innerHTML = `Текущая цена: <b>\$${input}</b>`;

    if (!priceInputForm.contains(inputShower)) {
        priceInputForm.append(inputShower);
    }
}


function showError() {

    if (priceInputForm.contains(inputShower)) {
        inputShower.remove();
    }

    inputFormInput.style.border = '2px solid red';
    
    // решил текст также подсветить красным. хоть этого и небыло в ТЗ
    // так вроди красивее))
    inputFormInput.style.color = 'red';

    if (!priceInputForm.contains(errorShower)) {
        priceInputForm.append(errorShower);
    }
}

function resetInput() {
    inputLabel.classList.remove('input-label-has-value');
    inputShower.remove();
    errorShower.remove();
    inputFormInput.style.border = null;
    inputFormInput.value = null;
}


let priceInputForm = document.getElementById('price-input-form'),
    inputFormInput = priceInputForm.children[0],
    inputLabel = priceInputForm.children[1],
    inputShower = document.createElement('div'),
    errorShower = document.createElement('span'),
    inputShowerText = document.createElement('span'),
    inputShowerBtn = document.createElement('img');


errorShower.classList.add('error-shower');
errorShower.textContent = 'Please enter correct price';
inputShower.classList.add('input-shower');
inputShowerBtn.classList.add('reset-input');
inputShower.append(inputShowerText, inputShowerBtn);


inputFormInput.addEventListener('focus', ()=> {

    inputLabel.classList.add('input-label-has-value');
    inputFormInput.style.color = null;

    // пока не введено корректное значение, оставляю рамку красной
    // так логичней вроди)
    if (!(inputFormInput.style.border === '2px solid red')) {
    inputFormInput.style.border = '2px solid green'; }

    inputFormInput.select();
});


inputFormInput.addEventListener('focusout', ()=> {

    if (inputFormInput.value) {
        if (inputFormInput.value >= 0) {
            showInput(inputFormInput.value);
        } else {
            showError();
        }
    } else {
        resetInput();
    }
});

inputShowerBtn.addEventListener('click', resetInput);
