// Экранирование - это некий способ выделить специальные элементы/команды в теле строки, чтобы
// интерпретатор браузера мог их различить и правильно интерпретировать))
// Например, кавычки в тексте, или перенос строки, или обратный слеш - нужно дополнить знаком "\",
// и тогда интерпретатор поймет, что после него идет специальный символ, и обработает его корректно.

"use strict";

function correctDate(date) {
    // if (!date) return false;
    let dateArray = date.split("."),
        currentDate = new Date().getFullYear();
    return (dateArray[0] > 0 && dateArray[0] < 32 && dateArray[1] > 0 && dateArray[1] < 13 && dateArray[2] > 1900 && dateArray[2] <= currentDate) ? true : false;
}

function createNewUser() {

    return {
        firstName: prompt("Enter first name"),
        lastName: prompt("Enter last name"),

        birthday: (function () {
            let date = prompt("Enter your birthday: dd.mm.yyyy");
            if (!date) return null;
            while(!correctDate(date)) {
                date = prompt("Wrong date. Try again!");
            }
            return date;
        })(),
        // Поидее лучше бы сразу переформатировать дату рождения в правильный формат, и хранить в нем. Но в условии
        // задачи, было сохранить дату в исходном виде. По-этому, так и сделал. Только добавил верификацию как смог)

        get getBirthYear() {
            if(this.birthday) {
                return new Date(this.birthday.split(".").reverse().join(",")).getFullYear();
            } else {
                return null;
            }
        },
        get getPassword() {
            if (this.firstName && this.lastName && this.getBirthYear) {
                return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.getBirthYear);
            } else {
                return "Can't create password";
            }
        },

    //     Долго колупался, не мог понять как работают даты))
    //     Только сегодня вроди дошло + Никита прояснил. В итоге сделал два способа. Решил оставить оба, т.к. не знаю
    //     какой правильней. По идее, 2. Первый как-то костыльно выглядит. Но я не уверен))

        get getAge1() {
            if (!this.birthday) return "User birthday is not set";
            let currentDate = new Date(),
                birthDate = new Date(this.birthday.split(".").reverse().join(",")),
                age = Math.trunc((currentDate - birthDate)/1000/60/60/24/365.25);

            return `getAge1 - ${this.firstName} is ${age} years old`;
        },

        get getAge2() {
            if (!this.birthday) return "User birthday is not set";
            let currentDate = new Date(),
                birthDate = new Date(this.birthday.split(".").reverse().join(",")),
                age = currentDate.getFullYear() - birthDate.getFullYear(),
                checkDate = new Date(birthDate.getFullYear() + age, birthDate.getMonth(), birthDate.getDate());

            if (currentDate < checkDate) age--;
            return `getAge2 - ${this.firstName} is ${age} years old`;
        }
    }
}

let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getPassword);
console.log(newUser.getAge1);
console.log(newUser.getAge2);