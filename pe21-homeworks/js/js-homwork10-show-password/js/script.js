'use strict';

function showPassword(whichEye) {

    if (inputs[whichEye].value) {

        if (eyes[whichEye].classList.contains('fa-eye')) {

            eyes[whichEye].classList.remove('fa-eye');
            eyes[whichEye].classList.add('fa-eye-slash');

            openEye = document.createElement('span');
            openEye.textContent = `Пароль: ${inputs[whichEye].value}`;
            openEye.classList.add('password-open');
            inputWrapper[whichEye].append(openEye);
        } else {
            eyes[whichEye].classList.remove('fa-eye-slash');
            eyes[whichEye].classList.add('fa-eye');
            inputWrapper[whichEye].lastChild.remove();
        }
    }
}

let inputs = document.getElementsByClassName('password'),
    eyes = document.getElementsByClassName('icon-password'),
    inputWrapper = document.getElementsByClassName('input-wrapper'),
    submitBtn = document.getElementById('submitBtn'),
    openEye,
    errorSpan;

errorSpan = document.createElement('span');
errorSpan.textContent = 'Нужно ввести одинаковые значения';
errorSpan.style.color = 'red';

eyes[0].addEventListener('click', ()=> showPassword(0));
eyes[1].addEventListener('click', ()=> showPassword(1));

submitBtn.addEventListener('click', ()=> {

   if (inputs[0].value === inputs[1].value) {
       errorSpan.remove();
       setTimeout(()=> alert('You are welcome!'), 10);
   } else {
       submitBtn.before(errorSpan);
   }
});

