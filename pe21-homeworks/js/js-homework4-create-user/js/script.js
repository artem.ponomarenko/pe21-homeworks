"use strict";

// Метод объекта, это некая встроенная функция в структуру свойств объекта, которая позволяет
// производить манипуляции со свойствами объекта: добавлять новые, изменять, либо возвращать
// комбинации свойств и/или результаты манипуляций с ними.

function createNewUser() {

    return {
        firstName: prompt("Enter first name"),
        lastName: prompt("Enter last name"),
        get setFirstName() {
            return this.firstName;
        },
        set setFirstName(value) {
            return this.firstName = value;
        },
        get setLastName() {
            return this.lastName;
        },
        set setLastName(value) {
            return this.lastName = value;
        },
        get getLogin() {
            if (this.firstName && this.lastName) {
                return (this.firstName[0] + this.lastName).toLocaleLowerCase();
            } else {
                return "Can't create login"
            }
        }
    }
}

let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getLogin);

newUser.setFirstName = prompt("Enter first name outside function (check)");
newUser.setLastName = prompt("Enter last name outside function (check)");

// newUser.firstName = "Sasha";

console.log(newUser);
console.log(newUser.getLogin);

// Возникли некоторые сложности с вот этой задачей:
// "Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую."
// Пробовал добавлять свойства через Object.defineProperty, чтобы проставить writable false,
// Но при таком условии, значение свойства вообще нельзя перезаписать, ни напрямую, ни через сеттер.
// Сейчас свойства меняются через сеттер, но если обратиться к объекту напрямую, например
// раскоментить строку 39, то все-равно свойство она меняет значение без проблем.
// Как сделать чтоб строка 39 не могла менять напрямую - так и не понял.
// Может что-то неправильно сделал или понял?



