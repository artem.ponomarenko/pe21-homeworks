'use strict';

// Написать функцию для рекурсивного полного клонирования объекта (без единой передачи по ссылке, внутренняя вложенность свойств объекта может быть достаточно большой).
// Функция должна успешно копировать свойства в виде объектов и массивов на любом уровне вложенности.
//     В коде нельзя использовать встроенные механизмы клонирования, такие как функция Object.assign() или спред-оператор.

let company = {
    sales: [{
        name: 'John',
        salary: 1000
    }, {
        name: 'Alice',
        salary: 600
    }],

    development: {
        sites: [{
            name: 'Peter',
            salary: 2000
        }, {
            name: 'Alex',
            salary: 1800
        }],

        internals: [{
            name: 'Jack',
            salary: 1300
        }]
    }
};

// частично идею решения пришлось затырыть с хабра. сразу признаюсь - сам не мог додуматься как написать
// идея вроди была понятна, но как ее реализовать не додул пока не увидел пример))
// почитал вроди понял, только дополнил проверкой на массив с его копированием.

function cloneObject(sourceObject) {

    // поидее работает так:
    let clonedObject = {};

    // создаем пустой новый объект куда будем клонировать
    for (let val in sourceObject) {

        // проверяем если первый элемент объекта, также объект
        if (sourceObject[val] instanceof Object) {

            // если да, то вызываем рекурсивно эту же функцию для вложенного объекта
            clonedObject[val] = cloneObject(sourceObject[val]);
        }

        // проверяем, если вложенный элемент не объект, а массив
        if (Array.isArray(sourceObject[val])) {

            // если да, то мапим массив во вложенный элемент объекта
            clonedObject[val] = sourceObject[val].map(value => value);
        }

        // если не первое и не второе, то копируем элемент исходного объекта в новый объект
        clonedObject[val] = sourceObject[val];
    }
    return clonedObject;
}

let newObject = cloneObject(company);
console.log(company);
console.log(newObject);
console.log(newObject === company);

// вроди как-то даже работает))
// если честно не могу сказать, что прям на 100% понимаю как) немного путаюсь с val
// но, при проверке на равенство все-равно выдает false - что не так?