/*
Обработчик событий - я понимаю как метод, который мы пожем прикрутить
к любому элементу страницы, который запускается при наступлении события,
которое в него заложено. Он ждет событие (действие пользователя или
системное событие на странице), и, при его наступлении, запускает переданную
ему функцию, которая обрабатывает это событие.
 */

'use strict';

function showInput(input) {
    // hiding error if was
    errorShower.style.display = 'none';
    errorShower.textContent = null;
    // showing correct input
    // вот тут так и не смог побороть. не хочет жирным выделять,
    // хоть ты тресни)) и стронг пробовал и болд - не работает. Не понимаю..
    inputShowerText.innerHTML = `Текущая цена: <strong>\$${input}</strong>`;
    inputShower.style.display = 'flex';
    inputFormInput.style.border = '2px solid green';
}

function showError() {
    // hiding input if was
    inputShower.style.display = 'none';
    inputShowerText.textContent = null;
    // showing error
    errorShower.textContent = 'Please enter valid price';
    errorShower.style.display = 'block';
    inputFormInput.style.border = '2px solid red';
}

function resetInput() {
    // reseting input if pressed reset or empty input
    inputLabel.classList.remove('input-label-has-value');
    errorShower.style.display = 'none';
    inputShower.style.display = 'none';
    inputFormInput.style.border = null;
    inputFormInput.value = null;
}

/*
я тут решил, что не стоит сильно использовать querySelector.
Поидее, из того, что я слышал, прямое обращение к элементам должно
работать быстрее.

Также, я сделал не через добавление/удаление узлов.
В ТЗ это прямо не указано. Показалось, что так оптимальней и красивей.))
Получается, что я сразу читаю разметку и потом с ней уже играюсь.
А если создавать/удалять каждый раз, то и кода больше вроди, и действий
браузера больше.

Если нужно, я переделаю на удаление/добавление.
Провозился долго с стилями, чтоб получилось красиво)

Ира, подскажи как такие вещи делать правильно в жизни?
Добавлять узлы или прятать?
 */
let priceInputForm = document.getElementById('price-input-form'),
    inputFormInput = priceInputForm.children[0],
    inputLabel = priceInputForm.children[1],
    inputShower = priceInputForm.children[2],
    inputShowerText = priceInputForm.children[2].children[0],
    inputResetBtn = priceInputForm.children[2].children[1],
    errorShower = priceInputForm.children[3];

// when focusing input
inputFormInput.addEventListener('focus', ()=> {

    // moving label on top of input
    inputLabel.classList.add('input-label-has-value');

    // if error was before, we leave border red until correct
    if (!(inputFormInput.style.border === '2px solid red')) {
    inputFormInput.style.border = '2px solid green'; }

    // selecting current input on focus
    inputFormInput.select();
});

// unfocusing input
inputFormInput.addEventListener('focusout', ()=> {

    // checking if not empty
    if (inputFormInput.value) {

        // checking if price is valid
        if (+inputFormInput.value > 0) {
            showInput(inputFormInput.value);

        } else {
            showError();
        }

    } else {
        // if empty
        resetInput();
    }
});

// reseting input
inputResetBtn.addEventListener('click', resetInput);
