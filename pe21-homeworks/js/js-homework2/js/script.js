"use strict"

// Циклы в языках программирования нужны чтобы производить повторяющиеся операции и проверки

// Main TASK - Numbers divided by 5

let yourNumber = +prompt("Enter a number");

while (!Number.isInteger(yourNumber)) {
    yourNumber = +prompt("Wrong entry. Please try again");
}
// не смог сделать проверку на Esc. + преобразовывает null в 0. но проверку на 0 как-то некорректно сюда ставить.
// нужно было бы добавлять еще одну проверку как-то в while, но я не придумал как можно это красиво сделать.

console.log("Numbers divided by 5");
let noDiv5 = true;
for (let i = 1; i <= yourNumber; i++) {
    if(i % 5 === 0) {
        console.log(i);
        noDiv5 = false;
    }
}

if (noDiv5) console.log("Sorry, no numbers");


// Additional TASK - Prime numbers

let m = +prompt("Enter a number M");
let n = +prompt("Enter a number N");

while (!Number.isInteger(m) && !Number.isInteger(n)) {
    m = +prompt("Enter a number M");
    n = +prompt("Enter a number N");
}

console.log("Prime numbers");
let noSimple = true;

nextPrime:
for (let i = m; i <= n; i++) {

    for (let j = 2; j < i; j++) {
        if (i % j === 0) continue nextPrime;
    }
    noSimple = false;
    console.log(i);
}

if (noSimple) console.log("No Prime numbers in this range");


// Это был перый вариант почти собственного изобретения))
// Потом почитал тексты к занятиям и переделал.

// let noSimple = true;
// for (let k = m; k <= n; k++) {
//     if (k === 1) {
//         console.log(k);
//         noSimple = false;
//     } else if (k === 2) {
//         console.log(k);
//         noSimple = false;
//     } else {
//         let isSimple = true;
//         for (let d = 2; d < k; d++) {
//             if (k % d === 0) {
//                 isSimple = false;
//             }
//         }
//         if (isSimple) {
//             console.log(k);
//             noSimple = false;
//         }
//     }
// }
// if (noSimple) {
//     console.log("There is no simple numbers in this range!");
//}

alert("See console");