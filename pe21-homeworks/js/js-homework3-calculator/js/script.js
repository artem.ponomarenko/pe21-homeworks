'use strict';

function validateNumber(value) {
    while (isNaN(+value)) {
        value = prompt("Wrong entry. Try again!", value);
    }
    return +value;
}

function validateOperand(value) {
    while (value !== "+" && value !== "-" && value !== "/" && value !== "*") {
        value = prompt("Wrong operand. Try again!", value);
    }
    return value;
}

function calculate(a,b,operand) {
    let result;
    switch (operand) {
        case "+": result = a + b; break;
        case "-": result = a - b; break;
        case "*": result = a * b; break;
        case "/": result = a / b; break;
    }
    return result;
}

let a = prompt("Enter A");
a = validateNumber(a);

let b = prompt("Enter B");
b = validateNumber(b);

let operand = prompt("Enter operation: + - * /");
operand = validateOperand(operand);

console.log("Result is: " + calculate(a,b,operand));