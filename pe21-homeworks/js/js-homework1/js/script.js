// Теоретическое задание:
//
// 1) Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
// Главная разница между новым форматом переменных (let, const) и старым форматом (var) в области видимости
// переменных. Видимость let и const ограничена блоком применения (внутри фигурных скобок), в то время, как var
// виден практически везде в коде. Его область видимости ограничена лишь функциями. Таким образом код с var уязвимее к
// ошибкам разработчиков (непреднамеренной переинициализации).
// Переменная const - вообще не может быть переинициализирована, в отличии от var и let. Она применяется для
// статичных данных.
//
// 2) Почему объявлять переменную через var считается плохим тоном?
// Так как var более уязвим к переинициализации. Другими словами, нужно помнить каждую переменную, где она
// применяется, чтобы случайно ее не перезаписать. Так может легко произойти, когда над кодом работают несколько
// разработчиков. 

let name, age;

name = prompt("Enter your name please");

while (name === "" || !isNaN(parseInt(name)) || name.length < 2 )   {
    name = prompt("You haven't entered name. Please try again", name);
}

age = parseInt(prompt("Enter your age please"));

while (isNaN(age) || age < 0 || age > 130) {
    age = parseInt(prompt("Age is incorrect. Please try again", age));
}

if(age < 18) {
    alert("You are not allowed to visit this website");
}
else if (age >=18 && age <= 22) {
    let confirmedAge = confirm("Are you sure you want to continue?");
    if (!confirmedAge) {
        alert("You are not allowed to visit this website " + name + "!");
    } else {
        alert("Welcome " + name + "!");
    }
}
else {
    alert("Welcome " + name + "!");
}