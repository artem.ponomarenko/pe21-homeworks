'use strict';

function hideInactive() {

    tabsArray.forEach((item, index) => {
        if (!item.classList.contains('active')) {
            tabsContentChildren[index].hidden = true;
        } else {
            tabsContentChildren[index].hidden = false;
            active = item;
        }
    })
}


let tabs = document.getElementsByClassName('tabs'),
    tabsContentChildren = document.querySelectorAll('.tabs-content li'),
    tabsArray = [...document.querySelectorAll('.tabs li')],
    active;


hideInactive();

tabs[0].addEventListener('click', (ev)=> {

    if (active !== ev.target ) {
        active.classList.remove('active');

        let currentTab = ev.target;
        currentTab.classList.add('active');

    }

    hideInactive();

});
