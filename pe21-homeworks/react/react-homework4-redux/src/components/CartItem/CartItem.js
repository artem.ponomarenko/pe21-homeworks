import React from 'react';
import CloseButton from "../../utils/svg/CloseButton";
import './CartItem.scss';
import {useDispatch} from "react-redux";
import {toRemoveSet} from "../../redux/reducers/toRemove/toRemoveActions";
import {inCartDecrement, inCartIncrement} from "../../redux/reducers/inCart/inCartActions";
import PropTypes from 'prop-types';

const CartItem = ({id, name, price, url, sku, color, qty}) => {
  const dispatch = useDispatch();

  return (
    <div className='cart-item'>
      <img className='cart-item__image' src={url} alt={name}/>
      <div className='cart-item__info'>
        <p className='cart-item__name'>{name}</p>
        <span className='cart-item__color'>Color: {color}</span>
        <span className='cart-item__sku'>SKU: {sku}</span>
      </div>
      <div className='cart-item__right-container'>
        <button className='cart-item__remove'
                onClick={() => dispatch(toRemoveSet({id, name, price, url, sku, color, qty}))}>
          Remove
          <CloseButton height='12px' width='12px' fill='gray' className='cart-item__remove__x'/>
        </button>
        <div className='cart-item__price-container'>
          <span className='cart-item__price'>Price &#8372; {price}</span>
          <div className='cart-item__quantity-container'>
            <button className='cart-item__quantity__change' onClick={() => dispatch(inCartDecrement(id))}>-</button>
            <span className='cart-item__quantity'>x{qty}</span>
            <button className='cart-item__quantity__change' onClick={() => dispatch(inCartIncrement(id))}>+</button>
          </div>
        </div>
        <span className='cart-item__total-price'>Subtotal &#8372; {qty * price}</span>
      </div>
    </div>
  );
};

CartItem.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  qty: PropTypes.number.isRequired,
  price: PropTypes.number.isRequired,
  url: PropTypes.string.isRequired,
  sku: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
}

export default CartItem;