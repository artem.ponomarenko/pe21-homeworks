import React from 'react';
import './Modal.scss';
import PropTypes from 'prop-types';
import Svg from "../../utils/svg/Svg";

const Modal = ({header, closeButton, content, buttonOk, buttonCancel, closeModal, modalStyle}) => {
  return (
    <div className='modal' onClick={event => event.target.className === 'modal' && closeModal()}>
      <div className={'modal__body ' + modalStyle}>
        <div className='modal__header'>
          <h3>{header}</h3>
          {closeButton && <Svg type='CloseButton' width='20px' height='20px' fill='white' onClick={closeModal}/>}
        </div>
        <div className='modal__text'>{content}</div>
        <div className='modal__buttons-container'>
          {buttonCancel}
          {buttonOk}
        </div>
      </div>
    </div>
  );
};


Modal.propTypes = {
  header: PropTypes.string.isRequired,
  closeButton: PropTypes.bool,
  content: PropTypes.object.isRequired,
  buttonOk: PropTypes.object.isRequired,
  buttonCancel: PropTypes.object.isRequired,
  closeModal: PropTypes.func.isRequired,
  modalStyle: PropTypes.string.isRequired
};

Modal.defaultProps = {
  closeButton: true
};

export default Modal;