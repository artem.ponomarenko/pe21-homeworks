import React from 'react';
import './ProductItem.scss';
import PropTypes from 'prop-types';
import Svg from "../../utils/svg/Svg";
import {useDispatch, useSelector} from "react-redux";
import {favoritesRemove, favoritesAdd} from "../../redux/reducers/favorites/favoritesActions";
import {addNewSet} from "../../redux/reducers/addNew/addNewActions";

const ProductItem = ({id, name, price, url, sku, color}) => {

  const dispatch = useDispatch();
  const cart = useSelector(store => store.inCart);
  const favorites = useSelector(store => store.favorites);
  const inCart = cart.find(item => item.id === id);
  const fav = favorites.some(item => item.id === id);

  return (
    <div className='product'>
      <img className='product__image' src={url} alt={name}/>
      <p className='product__name'>{name}</p>
      <div className='product__info'>
        <span className='product__color'>Color: {color}</span>
        <span className='product__sku'>SKU: {sku}</span>
      </div>
      <div className='product__price-btn'>
        <span className='product__price'>&#8372; {price}</span>
        <div className='product__price-btn'>
          {fav ?
            <Svg
              type='Fav'
              className='product__fav'
              width='20px'
              height='20px'
              fill='black'
              onClick={() => dispatch(favoritesRemove(id))}
            /> :
            <Svg
              type='FavEmpty'
              className='product__fav'
              width='20px'
              height='20px'
              fill='black'
              onClick={() => dispatch(favoritesAdd(id))}
            />
          }
          <button
            className={inCart ? 'product__button__in-cart' : 'product__button__add'}
            onClick={() => dispatch(addNewSet({id, name, url, price, color, sku, qty: 1}))}>
            {inCart ? 'In cart' : 'ADD TO CART'}
          </button>
        </div>
      </div>
    </div>
  );
};

ProductItem.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  url: PropTypes.string.isRequired,
  sku: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
};

export default ProductItem;