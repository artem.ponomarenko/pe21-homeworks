import React from 'react';
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import {inCartRemove} from "../../redux/reducers/inCart/inCartActions";
import {toRemoveReset} from "../../redux/reducers/toRemove/toRemoveActions";

const RemoveFromCartModal = () => {
  const dispatch = useDispatch();
  const {id, url, name, color, sku, price} = useSelector(store => store.toRemove);
  const buttonRemove =
    <Button
      onClick={() => dispatch(inCartRemove(id))}
      text='YES, REMOVE'
      backgroundColor='darkred'
      color='white'
    />;
  const buttonCancel =
    <Button
      onClick={() => dispatch(toRemoveReset())}
      text='Not really'
      backgroundColor='white'
      color='darkred'
    />;
  const product =
    <div className='add-product'>
      <img className='add-product__image' src={url} alt={name}/>
      <div className='add-product__container'>
        <p className='add-product__name'>{name}</p>
        <p className='add-product__color'>Color: {color}</p>
        <p className='add-product__sku'>SKU: {sku}</p>
        <p className='add-product__price'>Price: {price}</p>
      </div>
    </div>;
  return (
    <Modal
      content={product}
      buttonOk={buttonRemove}
      closeModal={() => dispatch(toRemoveReset())}
      buttonCancel={buttonCancel}
      modalStyle='remove-product'
      header='Sure, you want to remove?'
    />
  );
};

export default RemoveFromCartModal;