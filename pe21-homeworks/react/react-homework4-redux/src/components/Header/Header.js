import React from 'react';
import Svg from "../../utils/svg/Svg";
import './Header.scss';
import {NavLink} from "react-router-dom";
import HeaderIcon from "../HeaderIcon/HeaderIcon";
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

const Header = ({favNumber, inCartNumber}) => {
  return (
    <div className='header'>
      <Svg fill='black' width='180px' type='WwLogo'/>
      <div className='header__links'>
        <NavLink exact to='/products' className='header__link-item' activeClassName='header__link-item__active'>
          Products
        </NavLink>
        <NavLink exact to='/favorites' className='header__link-item'
                 activeClassName='header__link-item__active'>
          <HeaderIcon type='FavEmpty' number={favNumber}/>
        </NavLink>
        <NavLink exact to='/cart' className='header__link-item' activeClassName='header__link-item__active'>
          <HeaderIcon type='CartIcon' number={inCartNumber}/>
        </NavLink>
      </div>
    </div>
  );
};

const mapStateToProps = store => ({
  favNumber: store.favorites.length,
  inCartNumber: store.inCart.length,
});

Header.propTypes = {
  favNumber: PropTypes.number.isRequired,
  inCartNumber: PropTypes.number.isRequired,
}

export default connect(mapStateToProps)(Header);