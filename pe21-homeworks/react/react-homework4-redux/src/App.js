import React from 'react';
import './styles/App.css';
import Header from "./components/Header/Header";
import AppRoutes from "./routes/AppRoutes";
import Modals from "./Pages/Modals/Modals";

const App = () => {
    return (
        <div className="App">
            <Header/>
            <AppRoutes/>
            <Modals/>
        </div>
    )
};

export default App;
