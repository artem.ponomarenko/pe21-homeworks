import React from 'react';
import CartItem from "../../components/CartItem/CartItem";
import './Cart.scss';
import {useSelector} from 'react-redux';

const Cart = () => {
    const inCart = useSelector(store => store.inCart);

    const calculateTotal = (cart) => {
        const red = (acc, currentVal) => acc + currentVal.qty * currentVal.price;
        return cart.reduce(red, 0);
    }

    let cartCards = inCart.map(item => {
        return <CartItem
            {...item}
            key={item.id}
        />
    });

    return (
        <>
            {cartCards.length > 0 ?
                <div className='cards-container'>
                    {cartCards}
                    <p className='cart-total'>
                        Total amount &#8372; {calculateTotal(inCart)}
                    </p>
                </div> :
                <div className='products-container'>
                    <p className='no-favorites'>Your cart is empty yet</p>
                </div>
            }
        </>
    );
};

export default Cart;