import React from 'react';
import AddNewProdModal from "../../components/AddNewProdModal/AddNewProdModal";
import {useSelector} from "react-redux";
import RemoveFromCartModal from "../../components/RemoveFromCartModal/RemoveFromCartModal";

const Modals = () => {
  const addNew = useSelector(store => store.addNew);
  const toRemove = useSelector(store => store.toRemove);
  return (
    <>
      {addNew && <AddNewProdModal/>}
      {toRemove && <RemoveFromCartModal/>}
    </>
  );
};

export default Modals;