import React from 'react';
import ProductItem from "../../components/ProductItem/ProductItem";
import './Favorites.scss';
import {connect} from "react-redux";

const Favorites = ({favorites}) => {

  let favCards = favorites.map(item => {
    return <ProductItem
      {...item}
      key={item.id}
    />
  });

  return (
    <>
      <div className='products-container'>
        {
          favCards.length > 0 ?
            favCards :
            <p className='no-favorites'>You haven't selected favorites yet</p>
        }
      </div>
    </>
  );
};

const mapStateToProps = state => ({
  favorites: state.favorites,
});

export default connect(mapStateToProps, null)(Favorites);