import React from 'react';
import Fav from "./Fav";
import FavEmpty from "./FavEmpty";
import CloseButton from "./CloseButton";
import PropTypes from 'prop-types';
import WwLogo from "./WwLogo";
import CartIcon from "./CartIcon";

const Svg = (props) => {
    switch (props.type) {
        case 'Fav' : return <Fav {...props}/>;
        case 'FavEmpty' : return <FavEmpty {...props}/>;
        case 'CloseButton' : return <CloseButton {...props}/>;
        case 'WwLogo' : return <WwLogo {...props}/>;
        case 'CartIcon' : return <CartIcon {...props}/>;
        default : return null
    }
};

Svg.propTypes = {
    type: PropTypes.string.isRequired,
    width: PropTypes.string.isRequired,
    height: PropTypes.string,
    fill: PropTypes.string,
    onClick: PropTypes.func,
    className: PropTypes.string,
};

Svg.defaultProps = {
    onClick: ()=>{},
    className: null,
    fill: 'black'
};

export default Svg;