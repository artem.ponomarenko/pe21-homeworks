const initialStore = {
  products: {
    loading: false,
    error: null,
    result: []
  },
  favorites: JSON.parse(localStorage.getItem('favorites')) || [],
  inCart: JSON.parse(localStorage.getItem('cart')) || [],
  addNew: null,
  toRemove: null,
};

export default initialStore;