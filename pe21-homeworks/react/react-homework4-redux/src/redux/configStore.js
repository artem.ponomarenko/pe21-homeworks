import {applyMiddleware, compose, createStore} from "redux";
import thunk from "redux-thunk";
import initialStore from "./initialStore";
import rootReducer from "./rootReducer";

const store = createStore(
  rootReducer,
  initialStore,
  compose(
    applyMiddleware(thunk),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
  // composeWithDevTools(applyMiddleware(thunk))
  // вот это не сработало у меня почему-то. Где то нарушен синтаксис.
);

export default store;