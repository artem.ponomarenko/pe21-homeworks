import {IN_CART_SET} from "./inCartActions";
import initialStore from "../../initialStore";

export default function inCartReducer(cart = initialStore.inCart, {type, payload}) {
  switch (type) {
    case IN_CART_SET: {
      localStorage.setItem('cart', JSON.stringify(payload));
      return payload;
    }
    default:
      return cart;
  }
}