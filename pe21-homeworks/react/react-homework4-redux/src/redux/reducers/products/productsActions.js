import axios from 'axios';

export const PRODUCTS_LOADING = 'PRODUCTS_LOADING';
export const PRODUCTS_LOADED = 'PRODUCTS_LOADED';
export const PRODUCTS_ERROR = 'PRODUCTS_ERROR';

export function productsLoad() {
  return (dispatch) => {
    dispatch({type: PRODUCTS_LOADING});

    axios('/products.json')
      .then(res => {
        setTimeout(() => {
          dispatch({type: PRODUCTS_LOADED, result: res.data})
        }, 1000);
      })
      .catch(error => {
        dispatch({type: PRODUCTS_ERROR, error})
      })
  }
}