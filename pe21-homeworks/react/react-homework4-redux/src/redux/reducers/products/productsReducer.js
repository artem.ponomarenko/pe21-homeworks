import {PRODUCTS_ERROR, PRODUCTS_LOADED, PRODUCTS_LOADING} from "./productsActions";

export default function productsReducer(products, {type, error, result}) {
  switch (type) {
    case PRODUCTS_LOADING :
      return {...products, loading: true};
    case PRODUCTS_LOADED :
      return {...products, loading: false, result};
    case PRODUCTS_ERROR :
      return {...products, loading: false, error};
    default :
      return {...products};
  }
}