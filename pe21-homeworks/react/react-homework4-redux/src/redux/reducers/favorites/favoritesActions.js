export const FAVORITES_SET = 'FAVORITES_SET';

export const favoritesRemove = id => (dispatch, getState) => {
  dispatch({
    type: FAVORITES_SET,
    payload: getState().favorites.filter(item => item.id !== id)
  });
};

export const favoritesAdd = id => (dispatch, getState) => {
  dispatch({
    type: FAVORITES_SET,
    payload: [...getState().favorites, getState().products.result.find(item => item.id === id)]
  });
};