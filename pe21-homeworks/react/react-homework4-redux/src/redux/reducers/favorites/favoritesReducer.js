import {FAVORITES_SET} from "./favoritesActions";
import initialStore from "../../initialStore";

export default function favoritesReducer(favorites = initialStore.favorites, {type, payload}) {
  switch (type) {
    case FAVORITES_SET: {
      localStorage.setItem('favorites', JSON.stringify(payload));
      return payload
    }
    default:
      return favorites;
  }
}