import {ADD_NEW_RESET, ADD_NEW_SET} from "./addNewActions";
import initialStore from "../../initialStore";

export default function addNewReducer(addNew = initialStore.addNew, {type, payload}) {
  switch (type) {
    case ADD_NEW_SET: {
      return payload;
    }
    case ADD_NEW_RESET:
      return null;
    default:
      return addNew;
  }
}
