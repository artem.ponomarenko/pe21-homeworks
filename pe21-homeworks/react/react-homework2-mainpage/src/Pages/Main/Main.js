import React, {Component} from 'react';
import axios from "axios";
import './Main.scss';
import ProductItem from "../../components/ProductItem/ProductItem";
import AddNewProdModal from "../../components/AddNewProdModal/AddNewProdModal";


class Main extends Component {
    state = {
        products : [],
        favorites : JSON.parse(localStorage.getItem('favorites')) || [],
        cart : JSON.parse(localStorage.getItem('cart')) || [],
        addNew: null
    };

    componentDidMount() {
        axios('/products.json')
            .then(res => {
                this.setState({products: res.data});
            });
    }

    toggleFavorites(id) {
        let fav = this.state.favorites;

        if(fav.find(item => item.id === id)) {
            fav = fav.filter(item => item.id !== id);
        } else {
            const favNewItem = this.state.products.find(item => item.id === id);
            fav.push(favNewItem);
        }
        this.setState({favorites : fav});
        localStorage.setItem('favorites', JSON.stringify(fav));
    }

    addToCart() {
        let cart = this.state.cart;
        const cartNewItem = this.state.addNew;
        this.closeAddNewModal();
        cart.push(cartNewItem);
        this.setState({cart});
        localStorage.setItem('cart', JSON.stringify(cart));
    }

    callAddNewModal(id) {
        const addNewProd = this.state.products.find(item => item.id === id);
        this.setState({addNew: addNewProd});
    }

    closeAddNewModal() {
        this.setState({addNew: null})
    }


    render() {
        const addNewProdModal =
            <AddNewProdModal
                closeModal={() => this.closeAddNewModal()}
                addProduct={()=> this.addToCart()}
                productItem={this.state.addNew}
            />;
        let productCards = this.state.products.map(item => {
            return <ProductItem
                {...item}
                key={item.id}
                fav={!!this.state.favorites.find(favItem => favItem.id === item.id)}
                inCart={!!this.state.cart.find(cartItem => cartItem.id === item.id)}
                toggleFavorites={()=>this.toggleFavorites(item.id)}
                addToCart={()=> this.callAddNewModal(item.id)}
            />
        });
        return (
            <>
                <h1 className='h1'>Products</h1>
                <div className='products-container'>
                    {productCards}
                </div>
                {this.state.addNew && addNewProdModal}
            </>
        );
    }
}

export default Main;