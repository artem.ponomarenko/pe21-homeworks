import React, {Component} from 'react';
import './Button.scss';
import PropTypes from 'prop-types';

class Button extends Component {
    render() {
        const {backgroundColor, color, text, onClick} = this.props;
        return (
            <button
                className='button'
                style={{backgroundColor, color}}
                onClick={onClick}>
                {text}
            </button>
        );
    }
}

Button.propTypes = {
  backgroundColor: PropTypes.string,
    color: PropTypes.string,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
};

Button.defaultProps = {
    backgroundColor: 'black',
    color: 'white'
};

export default Button;