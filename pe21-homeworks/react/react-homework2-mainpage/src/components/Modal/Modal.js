import React, {Component} from 'react';
import './Modal.scss';
import PropTypes from 'prop-types';

class Modal extends Component {
    render() {
        const {header, closeButton, text, buttonOk, buttonCancel, onClick, modalStyle} = this.props;
        return (
                <div className='modal' onClick={event => event.target.className === 'modal' && onClick() }>
                    <div className={'modal__body '+ modalStyle}>
                        <div className='modal__header'>
                            <h3>{header}</h3>
                            {closeButton && <img src="/close-modal.svg" alt="Close" onClick={onClick}/>}
                        </div>
                        <div className='modal__text'>{text}</div>
                        <div className='modal__buttons-container'>
                            {buttonCancel}
                            {buttonOk}
                        </div>
                    </div>
                </div>
            );
        }
}

Modal.propTypes = {
  header: PropTypes.string.isRequired,
    closeButton: PropTypes.bool,
    text: PropTypes.element.isRequired,
    buttonOk: PropTypes.object.isRequired,
    buttonCancel: PropTypes.object.isRequired,
    onClick: PropTypes.func.isRequired,
    modalStyle: PropTypes.string.isRequired
};

Modal.defaultProps = {
  closeButton: true
};

export default Modal;