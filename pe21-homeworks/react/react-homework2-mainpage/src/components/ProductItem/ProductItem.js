import React, {Component} from 'react';
import './ProductItem.scss';
import PropTypes from 'prop-types';

class ProductItem extends Component {
    state = {
      inCart: this.props.inCart
    };

    render() {
        const {id, name, price, url, sku, color, fav, inCart, toggleFavorites, addToCart} = this.props;
        return (
            <div className='product'>
                <img className='product__image' src={url} alt={name}/>
                <p className='product__name'>{name}</p>
                <div className='product__info'>
                    <span className='product__color'>Color: {color}</span>
                    <span className='product__sku'>SKU: {sku}</span>
                </div>
                <div className='product__price-btn'>
                    <span className='product__price'>UAH {price}</span>
                    <div className='product__price-btn'>
                        <img
                            className='product__fav'
                            src={fav ? '/img/icons/fav.svg' : '/img/icons/fav-empty.svg'}
                            alt="Add to favourites"
                            onClick={()=> toggleFavorites(id)}
                        />
                        <button
                            className={inCart ? 'product__button__in-cart' : 'product__button__add'}
                            onClick={() => addToCart(id)}
                        >
                            {inCart ? 'In cart' : 'Add to cart'}
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

ProductItem.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    url: PropTypes.string.isRequired,
    sku: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    fav: PropTypes.bool,
    inCart: PropTypes.bool,
    toggleFavorites: PropTypes.func.isRequired,
    addToCart: PropTypes.func.isRequired
};

ProductItem.defaultProps = {
    fav: false,
    inCart: false
};

export default ProductItem;