import React, {Component} from 'react';
import './App.css';
import Main from "./Pages/Main/Main";

class App extends Component {
    state = {
        isAlertActive: false,
        isInfoActive: false,
        idAddNewActive: false
};
    render() {
        return (
            <div className="App">
                <Main/>
            </div>
        )
    }
}

export default App;
