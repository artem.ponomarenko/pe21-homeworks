import React, {Component} from 'react';
import Button from "../Button/Button";
import Modal from "../Modal/Modal";

class InfoModal extends Component {
    render() {
        const {onClick} = this.props;
        const buttonOk =
            <Button
                text={'Click me'}
                backgroundColor={'#4169e1'}
                onClick={onClick}
            />;
        const buttonCancel =
            <Button
                text={'Don\'t click me'}
                backgroundColor={'#3152b3'}
                onClick={onClick}
            />;
        return (
            <Modal
                header = {'Close me now!'}
                text = {'You can close me, but you can\'t erase me!'}
                closeButton = {false}
                buttonOk = {buttonOk}
                buttonCancel = {buttonCancel}
                onClick = {onClick}
                modalStyle = {'info'}
            />
        )
    }
}

export default InfoModal;