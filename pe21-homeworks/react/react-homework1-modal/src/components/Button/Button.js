import React, {Component} from 'react';
import './Button.scss';

class Button extends Component {
    render() {
        const {backgroundColor, width, text, onClick} = this.props;
        return (
            <button
                className='button'
                style={{backgroundColor, width}}
                onClick={onClick}>
                {text}
            </button>
        );
    }
}
export default Button;