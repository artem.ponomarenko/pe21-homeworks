import React, {Component} from 'react';
import Button from "../Button/Button";
import Modal from "../Modal/Modal";

class AlertModal extends Component {
    render() {
        const {onClick} = this.props;
        const buttonOk =
            <Button
                text={'Ok'}
                backgroundColor={'#cc3333'}
                width={'80px'}
                onClick={onClick}
            />;
        const buttonCancel =
            <Button
                text={'Cancel'}
                backgroundColor={'#b3382c'}
                width={'80px'}
                onClick={onClick}
            />;
        return (
            <Modal
                header = {'Do you want to delete this file?'}
                text = {'Once you delete this, it won\'t be possible to undo this action. Are you sure you want to' +
                ' delete this?'}
                closeButton = {true}
                buttonOk = {buttonOk}
                buttonCancel = {buttonCancel}
                onClick = {onClick}
                modalStyle = {'alert'}
            />
        )
    }
}

export default AlertModal;