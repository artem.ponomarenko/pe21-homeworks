import React, {Component} from 'react';
import './Modal.scss';

class Modal extends Component {
    render() {
        const {header, closeButton, text, buttonOk, buttonCancel, onClick, modalStyle} = this.props;
        return (
                <div className='modal' onClick={onClick}>
                    <div className={'modal__body '+ modalStyle}>
                        <div className='modal__header'>
                            <h3>{header}</h3>
                            {closeButton && <img src="/close-modal.svg" alt="Close" onClick={onClick}/>}
                        </div>
                        <p className='modal__text'>{text}</p>
                        <div className='modal__buttons-container'>
                            {buttonOk}
                            {buttonCancel}
                        </div>
                    </div>
                </div>
            );
        }
}
export default Modal;