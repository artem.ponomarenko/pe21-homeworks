import React, {Component} from 'react';
import './App.css';
import Button from "./components/Button/Button";
import AlertModal from "./components/AlertModal/AlertModal";
import InfoModal from "./components/InfoModal/InfoModal";

class App extends Component {
    state = {
        isAlertActive: false,
        isInfoActive: false
};
    toggleAlertModal() {
        this.setState({isAlertActive: !this.state.isAlertActive});
    }
    toggleInfoModal() {
        this.setState({isInfoActive: !this.state.isInfoActive});
    }
    render() {
        return (
            <div className="App">
                <div className='buttons-container'>
                    <Button
                        backgroundColor={'#d44637'}
                        text={'Open Alert modal'}
                        onClick={()=> this.toggleAlertModal()}
                    />
                    <Button
                        backgroundColor={'#395eff'}
                        text={'Open Info modal'}
                        onClick={()=> this.toggleInfoModal()}
                    />
                </div>
                {this.state.isAlertActive && <AlertModal onClick={() => this.toggleAlertModal()}/>}
                {this.state.isInfoActive && <InfoModal onClick={() => this.toggleInfoModal()}/>}
            </div>
        )
    }
}

export default App;
