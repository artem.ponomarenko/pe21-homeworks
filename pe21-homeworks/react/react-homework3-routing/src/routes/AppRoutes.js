import React from 'react';
import Main from "../Pages/Main/Main";
import Favorites from "../Pages/Favorites/Favorites";
import Cart from "../Pages/Cart/Cart";
import {Redirect, Switch, Route} from "react-router-dom";

const AppRoutes = () => {
    return (
        <Switch>
            <Route exact path='/' render={()=> <Redirect to='/products'/>}/>
            <Route exact path='/products' component={Main}/>
            <Route exact path='/favorites' component={Favorites}/>
            <Route exact path='/cart' component={Cart}/>
            <Route path='*' render={()=> <Redirect to='/products'/>}/>
        </Switch>
    );
};

export default AppRoutes;