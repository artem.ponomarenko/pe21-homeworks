import React from 'react';
import './ProductItem.scss';
import PropTypes from 'prop-types';
import Svg from "../../utils/svg/Svg";

const ProductItem = ({id, name, price, url, sku, color, fav, inCart, toggleFavorites, addToCart}) => {
    return (
        <div className='product'>
            <img className='product__image' src={url} alt={name}/>
            <p className='product__name'>{name}</p>
            <div className='product__info'>
                <span className='product__color'>Color: {color}</span>
                <span className='product__sku'>SKU: {sku}</span>
            </div>
            <div className='product__price-btn'>
                <span className='product__price'>&#8372; {price}</span>
                <div className='product__price-btn'>
                    {fav ?
                        <Svg
                            type='Fav'
                            className='product__fav'
                            width='20px'
                            height='20px'
                            fill='black'
                            onClick={() => toggleFavorites(id)}
                        /> :
                        <Svg
                            type='FavEmpty'
                            className='product__fav'
                            width='20px'
                            height='20px'
                            fill='black'
                            onClick={() => toggleFavorites(id)}
                        />
                    }
                    <button
                        className={inCart ? 'product__button__in-cart' : 'product__button__add'}
                        onClick={() => addToCart(id)}
                    >
                        {inCart ? 'In cart' : 'Add to cart'}
                    </button>
                </div>
            </div>
        </div>
    );
};

ProductItem.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    url: PropTypes.string.isRequired,
    sku: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    fav: PropTypes.bool,
    inCart: PropTypes.bool,
    toggleFavorites: PropTypes.func.isRequired,
    addToCart: PropTypes.func.isRequired
};

ProductItem.defaultProps = {
    fav: false,
    inCart: false
};

export default ProductItem;