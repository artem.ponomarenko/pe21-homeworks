import React from 'react';
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import './AddNewProdModal.scss';
import PropTypes from 'prop-types';

const AddNewProdModal = ({addProduct, closeModal, productItem}) => {
    const buttonAdd =
        <Button
            text='ADD TO CART'
            backgroundColor='black'
            color='white'
            onClick={addProduct}
        />;
    const buttonCancel =
        <Button
            text='Cancel'
            backgroundColor='white'
            color='black'
            onClick={closeModal}
        />;
    const product =
        <div className='add-product'>
            <img className='add-product__image' src={productItem.url} alt={productItem.name}/>
            <div className='add-product__container'>
                <p className='add-product__name'>{productItem.name}</p>
                <p className='add-product__color'>Color: {productItem.color}</p>
                <p className='add-product__sku'>SKU: {productItem.sku}</p>
                <p className='add-product__price'>Price: {productItem.price}</p>
            </div>
        </div>;
    return (
        <Modal
            header='Add this product to cart?'
            text={product}
            closeButton={true}
            buttonOk={buttonAdd}
            buttonCancel={buttonCancel}
            onClick={closeModal}
            modalStyle='add-new-product'
        />
    )
};

AddNewProdModal.propTypes = {
    closeModal: PropTypes.func.isRequired,
    addProduct: PropTypes.func.isRequired,
    productItem: PropTypes.object
};

AddNewProdModal.defaultProps = {
    productItem: null
};

export default AddNewProdModal;