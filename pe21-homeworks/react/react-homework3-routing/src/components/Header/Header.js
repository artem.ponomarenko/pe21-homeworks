import React from 'react';
import Svg from "../../utils/svg/Svg";
import './Header.scss';
import {NavLink} from "react-router-dom";

const Header = () => {
    return (
        <div className='header'>
            <Svg fill='black' width='180px' type='WwLogo'/>
            <div className='header__links'>
                <NavLink exact to='/products' className='header__link-item' activeClassName='header__link-item__active'>
                    Products
                </NavLink>
                <NavLink exact to='/favorites' className='header__link-item' activeClassName='header__link-item__active'>
                    <Svg fill='black' width='25px' height='25px' type='FavEmpty'/>
                </NavLink>
                <NavLink exact to='/cart' className='header__link-item' activeClassName='header__link-item__active'>
                    <Svg fill='black' width='25px' height='25px' type='CartIcon'/>
                </NavLink>
            </div>
        </div>
    );
};

export default Header;