import React from 'react';
import './Button.scss';
import PropTypes from 'prop-types';

const Button = ({backgroundColor, color, text, onClick}) => {
    return (
        <button
            className='button'
            style={{backgroundColor, color}}
            onClick={onClick}>
            {text}
        </button>
    );
};

Button.propTypes = {
    backgroundColor: PropTypes.string,
    color: PropTypes.string,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
};

Button.defaultProps = {
    backgroundColor: 'black',
    color: 'white'
};

export default Button;