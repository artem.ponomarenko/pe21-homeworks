import React from 'react';
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import PropTypes from "prop-types";

const RemoveFromCartModal = ({removeProduct, closeModal, productItem}) => {
    const buttonRemove =
        <Button
            onClick={removeProduct}
            text='YES, REMOVE'
            backgroundColor='darkred'
            color='white'
        />;
    const buttonCancel =
        <Button
            onClick={closeModal}
            text='Not really'
            backgroundColor='white'
            color='darkred'
        />;
    const product =
        <div className='add-product'>
            <img className='add-product__image' src={productItem.url} alt={productItem.name}/>
            <div className='add-product__container'>
                <p className='add-product__name'>{productItem.name}</p>
                <p className='add-product__color'>Color: {productItem.color}</p>
                <p className='add-product__sku'>SKU: {productItem.sku}</p>
                <p className='add-product__price'>Price: {productItem.price}</p>
            </div>
        </div>;
    return (
        <Modal
            text={product}
            buttonOk={buttonRemove}
            onClick={closeModal}
            buttonCancel={buttonCancel}
            modalStyle='remove-product'
            header='Sure you want to remove?'
        />
    );
};

RemoveFromCartModal.propTypes = {
    closeModal: PropTypes.func.isRequired,
    removeProduct: PropTypes.func.isRequired,
    productItem: PropTypes.object
};

RemoveFromCartModal.defaultProps = {
    productItem: null
};

export default RemoveFromCartModal;