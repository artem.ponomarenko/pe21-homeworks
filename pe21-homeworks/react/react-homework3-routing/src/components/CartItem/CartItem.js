import React from 'react';
import CloseButton from "../../utils/svg/CloseButton";
import './CartItem.scss';

const CartItem = ({id, name, price, url, sku, color, qty, removeFromCart, incrementQty, decrementQty}) => {
    return (
        <div className='cart-item'>
            <img className='cart-item__image' src={url} alt={name}/>
            <div className='cart-item__info'>
                <p className='cart-item__name'>{name}</p>
                <span className='cart-item__color'>Color: {color}</span>
                <span className='cart-item__sku'>SKU: {sku}</span>
            </div>
            <div className='cart-item__right-container'>
                <button className='cart-item__remove' onClick={() => removeFromCart(id)}>
                    Remove
                    <CloseButton height='12px' width='12px' fill='gray' className='cart-item__remove__x'/>
                </button>
                <div className='cart-item__price-container'>
                    <span className='cart-item__price'>Price &#8372; {price}</span>
                    <div className='cart-item__quantity-container'>
                        <button className='cart-item__quantity__change' onClick={()=>decrementQty(id)}>-</button>
                        <span className='cart-item__quantity'>x{qty}</span>
                        <button className='cart-item__quantity__change' onClick={()=>incrementQty(id)}>+</button>
                    </div>
                </div>
                <span className='cart-item__total-price'>Subtotal &#8372; {qty * price}</span>
            </div>
        </div>
    );
};

export default CartItem;