import React, {useEffect, useState, useRef} from 'react';
import ProductItem from "../../components/ProductItem/ProductItem";
import AddNewProdModal from "../../components/AddNewProdModal/AddNewProdModal";
import './Favorites.scss';
import axios from "axios";

const Favorites = () => {
    const [fav, setFav] = useState(JSON.parse(localStorage.getItem('favorites')) || []);
    const [inCart, setInCart] = useState(JSON.parse(localStorage.getItem('cart')) || []);
    const [addNew, setAddNew] = useState(null);
    let products = useRef([]);

    useEffect( () => {
        localStorage.setItem('favorites', JSON.stringify(fav));
        }, [fav]);

    useEffect( () => {
        localStorage.setItem('cart', JSON.stringify(inCart));
    }, [inCart]);

    function toggleFavorites(id) {
        const newFav = fav.filter(item => item.id !== id);
        setFav(newFav);
    }

    function callAddNewModal(id) {
        setAddNew(products.current.find(item => item.id === id));
    }

    function closeAddNewModal() {
        setAddNew(null);
    }

    function addToCart() {
        let cart = [...inCart];
        const cartNewItem = {...addNew, qty: 1};

        let cartExists = cart.find(item => item.id === cartNewItem.id);
        if(cartExists) {
            cartExists.qty++
        } else {
            cart.push(cartNewItem);
        }
        setInCart(cart);
        setAddNew(null);
    }

    let favCards = fav.map(item => {
        return <ProductItem
            {...item}
            key={item.id}
            fav={true}
            inCart={!!inCart.find(cartItem => cartItem.id === item.id)}
            toggleFavorites={() => toggleFavorites(item.id)}
            addToCart={() => callAddNewModal(item.id)}
        />
    });

    const addNewProdModal =
        <AddNewProdModal
            addProduct={() => addToCart()}
            closeModal={() => closeAddNewModal()}
            productItem={addNew}
        />;

    useEffect(()=> {
        axios('/products.json')
            .then(res => {
                products.current = res.data;
            });
    }, []);

    return (
        <>
            <div className='products-container'>
                {favCards.length > 0 ? favCards : <p className='no-favorites'>You have't selected favorites yet</p>}
            </div>
            {addNew && addNewProdModal}
        </>
    );
};

export default Favorites;