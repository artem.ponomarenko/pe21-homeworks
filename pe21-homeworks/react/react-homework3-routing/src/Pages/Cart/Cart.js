import React, {useEffect, useState} from 'react';
import CartItem from "../../components/CartItem/CartItem";
import './Cart.scss';
import RemoveFromCartModal from "../../components/RemoveFromCartModal/RemoveFromCartModal";

const Cart = () => {
    const [inCart, setInCart] = useState(JSON.parse(localStorage.getItem('cart')) || []);
    const [toRemove, setToRemove] = useState(null);

    useEffect(()=> {
        localStorage.setItem('cart', JSON.stringify(inCart));
    }, [inCart]);

    function removeFromCart() {
        const updatedInCart = inCart.filter(item => item.id !== toRemove.id);
        setInCart(updatedInCart);
        closeRemoveModal();
    }

    function calculateTotal(cart) {
        const red = (acc, currentVal) => acc + currentVal.qty * currentVal.price;
        return cart.reduce(red, 0);
    }

    function incrementQty(id) {
        const cart = [...inCart];
        cart[cart.findIndex(item => item.id === id)].qty++;
        setInCart(cart);
    }

    function decrementQty(id) {
        const cart = [...inCart];
        const productIndex = cart.findIndex(item => item.id === id);
        if (cart[productIndex].qty > 1) {
            cart[productIndex].qty--;
        } else {
            callRemoveModal(id);
            return;
        }
        setInCart(cart);
    }

    function callRemoveModal(id) {
        const removeProduct = inCart.find(item => item.id === id);
        setToRemove(removeProduct);
    }

    function closeRemoveModal() {
        setToRemove(null);
    }

    let cartCards = inCart.map(item => {
        return <CartItem
            {...item}
            key={item.id}
            removeFromCart={() => {callRemoveModal(item.id)}}
            incrementQty={() => {incrementQty(item.id)}}
            decrementQty={() => {decrementQty(item.id)}}
        />
    });

    return (
        <>
            {cartCards.length > 0 ?
                <div className='cards-container'>
                    {cartCards}
                    <p className='cart-total'>
                        Total amount &#8372; {calculateTotal(inCart)}
                    </p>
                </div> :
                <div className='products-container'>
                    <p className='no-favorites'>Your cart is empty yet</p>
                </div>
            }
            {toRemove && <RemoveFromCartModal
                removeProduct={removeFromCart}
                closeModal={closeRemoveModal}
                productItem={toRemove}/>
            }
        </>
    );
};

export default Cart;