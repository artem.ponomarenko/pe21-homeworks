import React, {useEffect, useState} from 'react';
import axios from "axios";
import './Main.scss';
import ProductItem from "../../components/ProductItem/ProductItem";
import AddNewProdModal from "../../components/AddNewProdModal/AddNewProdModal";


const Main = () => {
    const [products, setProducts] = useState([]);
    const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem('favorites')) || []);
    const [inCart, setInCart] = useState(JSON.parse(localStorage.getItem('cart')) || []);
    const [addNew, setAddNew] = useState(null);


    useEffect(() => {
        axios('/products.json')
            .then(res => {
                setProducts(res.data);
            });
    },[]);

    useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(favorites));
    }, [favorites]);

    useEffect(() => {
        localStorage.setItem('cart', JSON.stringify(inCart));
    }, [inCart]);

    function toggleFavorites(id) {
        let fav = [...favorites];

        if (fav.find(item => item.id === id)) {
            fav = fav.filter(item => item.id !== id);
        } else {
            const favNewItem = products.find(item => item.id === id);
            fav.push(favNewItem);
        }
        setFavorites(fav);
    }

    function addToCart() {
        let cart = [...inCart];
        const cartNewItem = {...addNew, qty: 1};

        let cartExists = cart.find(item => item.id === cartNewItem.id);
        if (cartExists) {
            cartExists.qty++
        } else {
            cart.push(cartNewItem);
        }
        closeAddNewModal();
        setInCart(cart);
    }

    function callAddNewModal(id) {
        const addNewProd = products.find(item => item.id === id);
        setAddNew(addNewProd);
    }

    function closeAddNewModal() {
        setAddNew(null);
    }

    const addNewProdModal =
        <AddNewProdModal
            closeModal={closeAddNewModal}
            addProduct={addToCart}
            productItem={addNew}
        />;

    let productCards = products.map(item => {
        return <ProductItem
            {...item}
            key={item.id}
            fav={!!favorites.find(favItem => favItem.id === item.id)}
            inCart={!!inCart.find(cartItem => cartItem.id === item.id)}
            toggleFavorites={() => toggleFavorites(item.id)}
            addToCart={() => callAddNewModal(item.id)}
        />
    });

    return (
        <>
            <div className='products-container'>
                {productCards}
            </div>
            {addNew && addNewProdModal}
        </>
    );
};

export default Main;