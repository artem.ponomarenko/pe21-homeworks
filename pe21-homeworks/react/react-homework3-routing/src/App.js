import React from 'react';
import './styles/App.css';
import Header from "./components/Header/Header";
import AppRoutes from "./routes/AppRoutes";

const App = () => {
    return (
        <div className="App">
            <Header/>
            <AppRoutes/>
        </div>
    )
};

export default App;
