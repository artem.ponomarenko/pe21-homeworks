import React from 'react';
import Svg from "../Icons/Svg";
import PropTypes from 'prop-types';
import './HeaderIcon.scss';

const HeaderIcon = ({type, number}) => {
  return (
    <div className='header-icon'>
      <Svg type={type} width='25px' height='25px' fill='black'/>
      {number > 0 && <span className='header-icon__number'>{number}</span>}
    </div>
  );
};

HeaderIcon.propTypes = {
  type: PropTypes.string.isRequired,
  number: PropTypes.number.isRequired,
};

export default HeaderIcon;