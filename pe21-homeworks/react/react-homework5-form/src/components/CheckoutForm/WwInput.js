import React from 'react';
import './WwInput.scss';

const WwInput = ({form, field, type, placeholder}) => {
  const {name} = field;

  let inputMessage = <span className='input__message'></span>

  if(form.touched[name]) {
    inputMessage = <span className='input__message correct'>Correct</span>
  }
  if (form.touched[name] && form.errors[name]) {
    inputMessage = <span className='input__message error'>{form.errors[name]}</span>;
  }

  return (
    <div className='input'>
      <span className='input__label'>{placeholder}</span>
      <input className='input__field' {...field} type={type}/>
      {inputMessage}
    </div>
  );
};

export default WwInput;