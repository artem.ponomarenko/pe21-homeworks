import React from 'react';
import {Field, Form, Formik} from 'formik';
import WwInput from "./WwInput";
import * as Yup from 'yup';
import './CheckoutForm.scss'
import {inCartCheckout} from "../../store/reducers/inCart/inCartActions";
import {useDispatch} from "react-redux";

const PHONE_NUM_REGEX = /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/;

const CheckoutFormSchema = Yup.object().shape({
  firstName: Yup.string()
    .min(2, 'First name must contain at least two letters')
    .max(40, 'First name can\'t be longer then 40 letters')
    .required('This field is required'),
  lastName: Yup.string()
    .min(2, 'Last name must contain at least two letters')
    .max(40, 'Last name is too long. No?')
    .required('This field is required'),
  age: Yup.number()
    .min(18, 'You must be adult to make orders')
    .max(150, 'You must be alive to make orders')
    .integer('Please enter correct age')
    .required('This field is required'),
  cellPhone: Yup.string()
    .matches(PHONE_NUM_REGEX, 'Please enter a correct phone number')
    .min(13, 'Please enter a correct phone number')
    .required('This field is required'),
})

const CheckoutForm = () => {
  const dispatch = useDispatch();

  return (
    <Formik
      initialValues={{
        firstName: '',
        lastName: '',
        age: '',
        cellPhone: '+380',
      }}
      validationSchema={CheckoutFormSchema}
      onSubmit={values => dispatch(inCartCheckout(values))}
    >
      {({handleSubmit, handleReset}) => (
          <Form className='checkout-form' onSubmit={handleSubmit}>
            <h1 className='checkout-form__header'>Checkout form</h1>
            <Field component={WwInput} type='text' name='firstName' placeholder='First Name'/>
            <Field component={WwInput} type='text' name='lastName' placeholder='Last Name'/>
            <Field component={WwInput} type='number' name='age' placeholder='Age'/>
            <Field component={WwInput} type='text' name='cellPhone' placeholder='Cell Phone'/>
            <button type='submit' className='checkout-form__submit-btn'>Checkout</button>
            <button type='button' className='checkout-form__reset-btn' onClick={handleReset}>Reset</button>
          </Form>
        )
      }
    </Formik>
  )
};

export default CheckoutForm;