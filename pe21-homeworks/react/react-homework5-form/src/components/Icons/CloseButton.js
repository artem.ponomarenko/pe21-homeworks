import React from 'react';

const CloseButton = ({width, height, fill, onClick, className}) => {
    return (
        <svg className={className} width={width} height={height} viewBox="0 0 20 20" fill={fill} onClick={()=>onClick()}>
            <path d="M20 2.01429L17.9857 0L10 7.98571L2.01429 0L0 2.01429L7.98571 10L0 17.9857L2.01429 20L10 12.0143L17.9857 20L20 17.9857L12.0143 10L20 2.01429Z" fill={fill}/>
        </svg>
    );
};
export default CloseButton;