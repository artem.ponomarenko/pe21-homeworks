import React from 'react';
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import './AddNewProdModal.scss';
import {useDispatch, useSelector} from "react-redux";
import {addNewReset} from "../../store/reducers/addNew/addNewActions";
import {inCartAdd} from "../../store/reducers/inCart/inCartActions";

const AddNewProdModal = () => {
  const addNew = useSelector(store => store.addNew);
  const dispatch = useDispatch();

  const buttonAdd =
    <Button
      text='ADD TO CART'
      backgroundColor='black'
      color='white'
      onClick={() => dispatch(inCartAdd(addNew))}
    />;
  const buttonCancel =
    <Button
      text='Cancel'
      backgroundColor='white'
      color='black'
      onClick={() => dispatch(addNewReset())}
    />;
  const product =
    <div className='add-product'>
      <img className='add-product__image' src={addNew.url} alt={addNew.name}/>
      <div className='add-product__container'>
        <p className='add-product__name'>{addNew.name}</p>
        <p className='add-product__color'>Color: {addNew.color}</p>
        <p className='add-product__sku'>SKU: {addNew.sku}</p>
        <p className='add-product__price'>Price: {addNew.price}</p>
      </div>
    </div>;
  return (
    <Modal
      header='Add this product to cart?'
      content={product}
      closeButton={true}
      buttonOk={buttonAdd}
      buttonCancel={buttonCancel}
      closeModal={() => dispatch(addNewReset())}
      modalStyle='add-new-product'
    />
  )
};

export default AddNewProdModal;