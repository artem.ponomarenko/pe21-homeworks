import {addNewReset} from "../addNew/addNewActions";
import {toRemoveReset, toRemoveSet} from "../toRemove/toRemoveActions";

export const IN_CART_SET = 'IN_CART_SET';

export const inCartSet = cart => ({type: IN_CART_SET, payload: cart});

export const inCartRemove = id => (dispatch, getState) => {
  let cart = [...getState().inCart];
  cart = cart.filter(item => item.id !== id);
  dispatch(inCartSet(cart));
  dispatch(toRemoveReset());
};

export const inCartAdd = productItem => (dispatch, getState) => {
  const cart = [...getState().inCart];
  const indexAdd = cart.findIndex(item => item.id === productItem.id);
  if (indexAdd === -1) {
    cart.push({...productItem, qty: 1})
  } else {
    cart[indexAdd].qty++
  }
  dispatch(inCartSet(cart));
  dispatch(addNewReset());
};

export const inCartIncrement = id => (dispatch, getState) => {
  const cart = [...getState().inCart];
  const index = cart.findIndex(item => item.id === id);
  cart[index].qty++
  dispatch(inCartSet(cart));
}

export const inCartDecrement = id => (dispatch, getState) => {
  const cart = [...getState().inCart];
  const index = cart.findIndex(item => item.id === id);
  if (cart[index].qty > 1) {
    cart[index].qty--
    dispatch(inCartSet(cart));
  } else {
    dispatch(toRemoveSet(cart[index]));
  }
}

export const inCartCheckout = values => (dispatch, getState) => {
  const cart = [...getState().inCart];
  const checkout = {
    user: values,
    bought: cart
  }
  console.log(checkout);
  dispatch(inCartSet([]));
}