export const TO_REMOVE_SET = 'TO_REMOVE_SET';
export const TO_REMOVE_RESET = 'TO_REMOVE_RESET';

export const toRemoveSet = (item) => ({type: TO_REMOVE_SET, payload: item});
export const toRemoveReset = () => ({type: TO_REMOVE_RESET});