import {TO_REMOVE_RESET, TO_REMOVE_SET} from "./toRemoveActions";
import initialStore from "../../initialStore";

export default function toRemoveReducer(toRemove = initialStore.toRemove, {type, payload}) {
  switch (type) {
    case TO_REMOVE_SET:
      return payload;
    case TO_REMOVE_RESET:
      return null;
    default:
      return toRemove;
  }
}