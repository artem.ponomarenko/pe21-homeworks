export const ADD_NEW_SET = 'ADD_NEW_SET';
export const ADD_NEW_RESET = 'ADD_NEW_RESET';

export const addNewSet = (item) => ({type: ADD_NEW_SET, payload: item});
export const addNewReset = () => ({type: ADD_NEW_RESET});