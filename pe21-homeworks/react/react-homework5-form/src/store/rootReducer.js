import {combineReducers} from "redux";
import productsReducer from "./reducers/products/productsReducer";
import favoritesReducer from "./reducers/favorites/favoritesReducer";
import inCartReducer from "./reducers/inCart/inCartReducer";
import addNewReducer from "./reducers/addNew/addNewReducer";
import toRemoveReducer from "./reducers/toRemove/toRemoveReducer";

export default combineReducers({
    products: productsReducer,
    favorites: favoritesReducer,
    inCart: inCartReducer,
    addNew: addNewReducer,
    toRemove: toRemoveReducer,
  }
);