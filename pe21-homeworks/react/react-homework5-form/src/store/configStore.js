import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";
import initialStore from "./initialStore";
import rootReducer from "./rootReducer";
import {composeWithDevTools} from "redux-devtools-extension";

const store = createStore(
  rootReducer,
  initialStore,
  composeWithDevTools(applyMiddleware(thunk))
);

export default store;