import React from 'react';
import CartItem from "../../components/CartItem/CartItem";
import './Cart.scss';
import {useSelector} from 'react-redux';
import CheckoutForm from "../../components/CheckoutForm/CheckoutForm";

const Cart = () => {
  const inCart = useSelector(store => store.inCart);

  const calculateTotal = (cart) => {
    const red = (acc, currentVal) => acc + currentVal.qty * currentVal.price;
    return cart.reduce(red, 0);
  }

  const cartCards = inCart.map(item => {
    return <CartItem
      {...item}
      key={item.id}
    />
  });

  return (
    cartCards.length > 0 ?
      <div className='cart'>
        <div className='cart__container'>
          <h1 className='cart__header'>Cart</h1>
          {cartCards}
          <p className='cart__total'>
            Total amount &#8372; {calculateTotal(inCart)}
          </p>
        </div>
        <CheckoutForm/>
      </div> :
      <div className='products-container'>
        <p className='no-favorites'>Your cart is empty yet</p>
      </div>
  );
};

export default Cart;