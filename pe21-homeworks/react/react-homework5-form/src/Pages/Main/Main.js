import React, {useEffect} from 'react';
import './Main.scss';
import {connect} from "react-redux";
import {productsLoad} from "../../store/reducers/products/productsActions";
import ProductItem from "../../components/ProductItem/ProductItem";

const Main = (
  {
    setProducts,
    products,
    productsLoading,
    favorites,
  }
) => {

  useEffect(() => {
    setProducts();
  }, []);

  let productCards = products.map(item => {
    return <ProductItem
      {...item}
      key={item.id}
      fav={!!favorites.find(favItem => favItem.id === item.id)}
    />
  });

  return (
    <>
      <div className='products-container'>
        {productCards}
      </div>
      {productsLoading && <div className='loader'></div>}
    </>
  );
};

const mapStateToProps = store => ({
  products: store.products.result,
  productsLoading: store.products.loading,
  favorites: store.favorites,
  inCart: store.inCart,
});

const mapDispatchToProps = dispatch => ({
  setProducts: products => dispatch(productsLoad(products)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);